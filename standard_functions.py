
#####################
# Import global modules
#####################
import os
import sys
import urllib2
import copy
from string import split
from string import upper
from os import linesep
from types import *
from datetime import datetime
#####################


#######################################################
# Standard Functions
#######################################################


def read_url_old(l_url):
    """
    Download webpage
    """
    # print "URL=", l_url
    req = urllib2.urlopen('%s' % l_url)
    l_text = req.readlines()
    return l_text


def read_url(l_url):
    """
    Download webpage
    """
    # print "URL=", l_url

    # y="""
    hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
           'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
           'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
           'Accept-Encoding': 'none',
           'Accept-Language': 'en-US,en;q=0.8',
           'Connection': 'keep-alive'}
    #"""

    #hdr = {'User-Agent': 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)'}

    req = urllib2.Request(l_url, headers=hdr)

    #req=urllib2.urlopen('%s' % l_url)
    req = urllib2.urlopen(req)
    l_text = req.readlines()
    return l_text


def read_url_with_lynx(l_url, tmp_file):
    """
    Download webpages with LYNX
    """
    #tmp_file="%s/term_tmp.file" % (l_dir)
    if file_exists(tmp_file):
        remove_command = "rm %s" % (tmp_file)
        os.system(remove_command)

    lynx_command = """ lynx -source '%s' > %s """ % (l_url, tmp_file)
    os.system(lynx_command)
    return


def read_url2(l_url):
    """
    Download webpage
    """
    # print "URL=", l_url
    req = urllib2.urlopen('%s' % l_url)
    l_text = req.read()

    return l_text


def read_file(L_file_name):
    """
    Read file, output lines
    """
    done_input1_switch = 0
    while not done_input1_switch:
        input_file0s = split(L_file_name)
        input_file1 = input_file0s[0]
        input_file1_exist = os.access(input_file1, os.F_OK)
        input_file1_read = os.access(input_file1, os.R_OK)
        if input_file1_exist == 0:
            print ("""\n ERROR: File with  name %s does not exist. 
 Please try again.\n: """ % input_file1)
            sys.exit(1)
        elif input_file1_read == 0:
            print("""\n ERROR: The program can not read the file.\n: """)
            sys.exit(1)
        else:
            done_input1_switch = 1
    file_open = open(L_file_name, 'r')
    file_lines = file_open.readlines()
    file_open.close()
    return(file_lines)


def make_dir_no_remove(dir_name):
    """
    Create directory if it does not exist
    """
    dir_exist = os.access(dir_name, os.F_OK)
    if dir_exist == 0:
        os.system('mkdir %s' % dir_name)
    return()


def make_dir(dir_name):
    """
    Create directory if it does not exist
    """
    dir_exist = os.access(dir_name, os.F_OK)
    if dir_exist == 0:
        os.system('mkdir %s' % dir_name)
    else:
        os.system('rm -r -f %s' % dir_name)
        os.system('mkdir %s' % dir_name)
    return()


def write_file(l_filename, l_text):
    """
    Write a name into a file
    """
    l_filename_open = open(l_filename, "w")
    l_filename_open.write(l_text)
    l_filename_open.close()
    return


def is_number(l_entry):
    """
    Function to determine if a string can be converted into a float. 
    Returns 1 if yes, 0 if no
    """
    l_result = 0
    try:
        a = float(l_entry)
        l_result = 1
    except:
        l_result = 0
    return(l_result)


def read_url(l_url):
    """
    Download webpage
    """
    # print "URL=", l_url
    req = urllib2.urlopen('%s' % l_url)
    l_text = req.readlines()
    return l_text


def file_exists(l_file):
    """
    Return 1 if file exists and 0 if it does not 
    """
    exists = os.access(l_file, os.F_OK)
    return exists


def import_local_modules(l_module_file, l_script_location, l_prefix):
    """
    Import Python dictionary
    """
    verbose = 0
    if verbose >= 1:
        print "Running function  import_local_modules()"
    if verbose >= 1:
        print "l_module_file= ", l_module_file
    l_module_base_file = os.path.basename(l_module_file)
    if len(l_module_base_file) > 3 and l_module_base_file[-3:] == ".py":
        l_module_file_base = "%s_%s" % (l_prefix, l_module_base_file[:-3])
    else:
        l_module_file_base = "%s_%s" % (l_prefix, l_module_base_file)
    module_py = "%s.py" % l_module_file_base
    module_pyc = "%s.pyc" % l_module_file_base
    if verbose >= 1:
        print "module_py= ", module_py
    if verbose >= 1:
        print "module_pyc= ", module_pyc
    if file_exists(module_py):
        os.system("rm %s " % module_py)
    if file_exists(module_pyc):
        os.system("rm %s " % module_pyc)

    module_local_filename, module_local_basename = copy_module_file(l_module_file, l_module_file_base, l_script_location)
    module_results = __import__(module_local_basename)

    reload(module_results)
    module_results_dic = copy.deepcopy(module_results.dic)
    return module_results_dic


def copy_module_file(l_module_full_path, l_module_basename, l_script_location):
    """
    Copy module file
    """
    verbose = 0
    local_module_name = "%s.py" % (l_module_basename)
    local_module_basename = "%s" % (l_module_basename)
    copy_command = "cp %s %s/%s" % (l_module_full_path, l_script_location, local_module_name)
    if os.access("rm %sc " % local_module_name, os.F_OK) == 1:
        os.system("rm %sc " % local_module_name)
    if os.access("rm %s " % local_module_name, os.F_OK) == 1:
        os.system("rm %s " % local_module_name)
    if verbose >= 11:
        print copy_command
        print "local_module_name=", local_module_name
        print "local_module_basename=", local_module_basename
    os.system(copy_command)

    return local_module_name, local_module_basename


def find_current_date():
    """
    Find current date
    """
    current_time = str(datetime.now())
    current_time = current_time.replace("-", " ")
    current_time = current_time.replace(":", " ")
    if verbose >= 1:
        print "current_time= ", current_time
    w = current_time.split()
    if verbose >= 1:
        print " current_time_split= ", w
    # current_time_split=  ['2013', '09', '16', '04', '11', '42.437021']
    month_number = int(w[1])
    day = int(w[2])
    year = int(w[0])
    real_hour, minutes = int(w[3]), int(w[4])

    return month_number, day, year


###################
# End of standard functions
###################
