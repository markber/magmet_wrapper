#######################################################
# Default
#######################################################
verbose=1
g_verbose=1
python_program="python"
location=""
project=""
directory_with_xml_files=""
directory_with_xml_files_default=""
env="mark_home"


setting_flag="--setting" # Done
update_flag="--update"
ReadIOSpectrum_path_argument="ReadIOSpectrum.path=" #Done
Quantify_path_argument="Quantify.path="#Done
Quantify_dss_conc_argument="Quantify.dss_conc="
AnnealedParticleMF_nr_samples_argument="AnnealedParticleMF.nr_samples="
SaveStatus_path_argument="SaveStatus.path="
SimplePlot_path_argument="SimplePlot.path="
Save_path_argument="Save.path="
SaveResultAsJson_json_file_argument="SaveResultAsJson.json_file="
LibReading_lib_folder_argument="LibReading.lib_folder="
Quantify_lb_file_argument="Quantify.lb_file="
SaveResultAsJson_confidence_scores_argument="SaveResultAsJson.confidence_scores=" 
SaveSpectrumXYPairs_path_argument="SaveSpectrumXYPairs.path=" 
SaveSpectrumXYPairs_xy_file_argument="SaveSpectrumXYPairs.xy_file=" 
LibReading_filter_file_argument="LibReading.filter_file=" 
LibReading_weight_list_argument="LibReading.weight_list=" 


#######################################################
# Dictionaries
#######################################################
env_dict={}
env_dict["sexsmith"]={}
env_dict["tmic_bayesil"]={}
env_dict["public_bayesil"]={}
env_dict["mark_home"]={}
env_dict["public_bayesil_clone"]={}


mark_home_command="""
python /home/mark/software/BayesilWorking/bayesil-app/Bayesil/bayesil.py #Done
 /home/mark/Dropbox/Fixing_Bayesil_TMIC_version/tmic_bayesil_web_profile_tmp #Done
--setting /home/mark/Dropbox/Fixing_Bayesil_TMIC_version/tmic_bayesil_setting  #Done
--update ReadIOSpectrum.path=/home/mark/Dropbox/Fixing_Bayesil_TMIC_version/tests_at_home/test_case_1/spectrum.fid  # Done
Quantify.path=/home/mark/Dropbox/Fixing_Bayesil_TMIC_version/tests_at_home/test_case_1 #Done
Quantify.dss_conc=1000.0 
AnnealedParticleMF.nr_samples=10000 
SaveStatus.path=/home/mark/Dropbox/Fixing_Bayesil_TMIC_version/tests_at_home/test_case_1 
SimplePlot.path=/home/mark/Dropbox/Fixing_Bayesil_TMIC_version/tests_at_home/test_case_1/plot.png 
Save.path=/home/mark/Dropbox/Fixing_Bayesil_TMIC_version/tests_at_home/test_case_1
SaveResultAsJson.json_file=/home/mark/Dropbox/Fixing_Bayesil_TMIC_version/tests_at_home/test_case_1/result.json 
LibReading.lib_folder=/home/mark/software/BayesilWorking/Libraries/500 
Quantify.lb_file=/home/mark/software/BayesilWorking/Libraries/filters/lower_bound_serum_exceptions.csv 
SaveResultAsJson.confidence_scores=/home/mark/software/BayesilWorking/Libraries/filters/confidence_scores.txt 
SaveSpectrumXYPairs.path=/home/mark/Dropbox/Fixing_Bayesil_TMIC_version/tests_at_home/test_case_1 
LibReading.filter_file=/home/mark/software/BayesilWorking/Libraries/filters/serum.txt  
LibReading.weight_list=weightSerum.csv
"""

#/home/mark/Dropbox/Fixing_Bayesil_TMIC_version/tests_at_home/nist


# Mark environement

env_dict["mark_home"]["project_location"]="/home/mark/Dropbox/Fixing_Bayesil_TMIC_version/tests_at_home/nist"
env_dict["mark_home"]["bayesil_app_location"]="/home/mark/software/BayesilWorking"



env_dict["mark_home"]["link_to_bayesil_program"]="/home/mark/software/BayesilWorking/bayesil-app/Bayesil/bayesil.py"
#env_dict["mark_home"]["link_to_job_file"]="/home/mark/Dropbox/Fixing_Bayesil_TMIC_version/tmic_bayesil_web_profile_tmp"
env_dict["mark_home"]["link_to_job_file"]="/home/mark/Dropbox/Fixing_Bayesil_TMIC_version/tmic_bayesil_web_profile_process"


env_dict["mark_home"]["link_to_setting"]="/home/mark/Dropbox/Fixing_Bayesil_TMIC_version/tmic_bayesil_setting"

#env_dict["mark_home"]["link_to_spectrum_dir"]="/home/mark/Dropbox/Fixing_Bayesil_TMIC_version/tests_at_home/test_case_1/spectrum.fid"
env_dict["mark_home"]["link_to_spectrum_dir"]="/home/mark/Dropbox/NMR_kit/700_serum/NIST.fid"


#env_dict["mark_home"]["Quantify_path"]="/home/mark/Dropbox/Fixing_Bayesil_TMIC_version/tests_at_home/test_case_1"
env_dict["mark_home"]["Quantify_path"]="%s" % env_dict["mark_home"]["project_location"]


env_dict["mark_home"]["Quantify_dss_conc"]=1000.0
env_dict["mark_home"]["AnnealedParticleMF_nr_samples"]=10000 

env_dict["mark_home"]["SaveStatus_path"]="%s" % env_dict["mark_home"]["project_location"]
env_dict["mark_home"]["SimplePlot_path"]="%s/plot.png" % env_dict["mark_home"]["project_location"]
env_dict["mark_home"]["Save_path"]="%s" % env_dict["mark_home"]["project_location"]
env_dict["mark_home"]["SaveResultAsJson_json_file"]="%s/result.json" % env_dict["mark_home"]["project_location"]
env_dict["mark_home"]["SaveSpectrumXYPairs_path"]="%s" % env_dict["mark_home"]["project_location"]
env_dict["mark_home"]["SaveSpectrumXYPairs_xy_file"]="xy_spectrum.txt"



env_dict["mark_home"]["LibReading_lib_folder"]="%s/Libraries/700" % env_dict["mark_home"]["bayesil_app_location"]
env_dict["mark_home"]["Quantify_lb_file"]="%s/Libraries/filters/lower_bound_serum_exceptions.csv" % env_dict["mark_home"]["bayesil_app_location"]
env_dict["mark_home"]["SaveResultAsJson_confidence_scores_file"]="%s/Libraries/filters/confidence_scores.txt" % env_dict["mark_home"]["bayesil_app_location"]
env_dict["mark_home"]["LibReading_filter_file"]="%s/Libraries/filters/serum.txt" % env_dict["mark_home"]["bayesil_app_location"]
env_dict["mark_home"]["LibReading_weight_list"]="weightSerum.csv"



# Sexsmith environement
env_dict["sexsmith"]["link_to_bayesil_program"]="/home/mkr/bayesil/bayesil-app/Bayesil/bayesil.py"
env_dict["sexsmith"]["link_to_job_file"]="/home/mkr/Documents/Mark/bayesil_web_profile_processing.job"
env_dict["sexsmith"]["link_to_setting"]="/home/mkr/bayesil/bayesil-app/bayesil-app/setting"
env_dict["sexsmith"]["link_to_spectrum_dir"]="/home/mkr/Documents/Mark/serum/test_dir/BPAB-40-w0.fid"
env_dict["sexsmith"]["Quantify_path"]="/home/mkr/Documents/Mark/serum/test_dir"
env_dict["sexsmith"]["Quantify_dss_conc"]=1000.0 
env_dict["sexsmith"]["AnnealedParticleMF_nr_samples"]=10000
env_dict["sexsmith"]["SaveStatus_path"]="/home/mkr/Documents/Mark/serum/test_dir"
env_dict["sexsmith"]["SimplePlot_path"]="/home/mkr/Documents/Mark/serum/test_dir/plot.png" 
env_dict["sexsmith"]["Save_path"]="/home/mkr/Documents/Mark/serum/test_dir"
env_dict["sexsmith"]["SaveResultAsJson_json_file"]="/home/mkr/Documents/Mark/serum/test_dir/result.json"
env_dict["sexsmith"]["LibReading_lib_folder"]="/home/mkr/bayesil/bayesil-app/Libraries/700"
env_dict["sexsmith"]["Quantify_lb_file"]="/home/mkr/bayesil/bayesil-app/Libraries/filters/lower_bound_serum_exceptions.csv"
env_dict["sexsmith"]["SaveResultAsJson_confidence_scores_file"]="/home/mkr/bayesil/bayesil-app/Libraries/filters/confidence_scores.txt"
env_dict["sexsmith"]["SaveSpectrumXYPairs_path"]="/home/mkr/Documents/Mark/serum/test_dir"
env_dict["sexsmith"]["SaveSpectrumXYPairs_xy_file"]="xy_spectrum.txt"
env_dict["sexsmith"]["LibReading_filter_file"]="/home/mkr/bayesil/bayesil-app/Libraries/filters/serum.txt"
env_dict["sexsmith"]["LibReading_weight_list"]="weightSerum.csv"


# tmic_bayesil environement
env_dict["tmic_bayesil"]["link_to_bayesil_program"]="/apps/bayesil/project/releases/20170815225908/lib/bayesil/app/Bayesil/bayesil.py"
env_dict["tmic_bayesil"]["link_to_job_file"]="/apps/bayesil/project/releases/20170815225908/lib/bayesil/jobs/bayesil_web_profile.job"
env_dict["tmic_bayesil"]["link_to_setting"]="/apps/bayesil/project/releases/20170815225908/lib/bayesil/app/setting"
env_dict["tmic_bayesil"]["link_to_spectrum_dir"]="/apps/bayesil/project/releases/20170815225908/BayesilWorking/Results/fbb7163ce52dad80e4e7faea4d0b6d74e22d7e13/11537/spectrum.fid"
env_dict["tmic_bayesil"]["Quantify_path"]="/apps/bayesil/project/releases/20170815225908/BayesilWorking/Results/fbb7163ce52dad80e4e7faea4d0b6d74e22d7e13/11537"
env_dict["tmic_bayesil"]["Quantify_dss_conc"]=1000.0 
env_dict["tmic_bayesil"]["AnnealedParticleMF_nr_samples"]=10000
env_dict["tmic_bayesil"]["SaveStatus_path"]="/apps/bayesil/project/releases/20170815225908/BayesilWorking/Results/fbb7163ce52dad80e4e7faea4d0b6d74e22d7e13/11537"
env_dict["tmic_bayesil"]["SimplePlot_path"]="/apps/bayesil/project/releases/20170815225908/BayesilWorking/Results/fbb7163ce52dad80e4e7faea4d0b6d74e22d7e13/11537/plot.png"
env_dict["tmic_bayesil"]["Save_path"]="/apps/bayesil/project/releases/20170815225908/BayesilWorking/Results/fbb7163ce52dad80e4e7faea4d0b6d74e22d7e13/11537"
env_dict["tmic_bayesil"]["SaveResultAsJson_json_file"]="/apps/bayesil/project/releases/20170815225908/BayesilWorking/Results/fbb7163ce52dad80e4e7faea4d0b6d74e22d7e13/11537/result.json"
env_dict["tmic_bayesil"]["LibReading_lib_folder"]="/apps/bayesil/project/releases/20170815225908/BayesilWorking/Libraries/700"
env_dict["tmic_bayesil"]["Quantify_lb_file"]="/apps/bayesil/project/releases/20170815225908/BayesilWorking/Libraries/filters/lower_bound_serum_exceptions.csv"
env_dict["tmic_bayesil"]["SaveResultAsJson_confidence_scores_file"]="/apps/bayesil/project/releases/20170815225908/BayesilWorking/Libraries/filters/confidence_scores.txt"
env_dict["tmic_bayesil"]["SaveSpectrumXYPairs_path"]="/apps/bayesil/project/releases/20170815225908/BayesilWorking/Results/fbb7163ce52dad80e4e7faea4d0b6d74e22d7e13/11537"
env_dict["tmic_bayesil"]["SaveSpectrumXYPairs_xy_file"]="xy_spectrum.txt"
env_dict["tmic_bayesil"]["LibReading_filter_file"]="/apps/bayesil/project/releases/20170815225908/lib/bayesil/metabolites/serum.txt"
env_dict["tmic_bayesil"]["LibReading_weight_list"]="weightSerum.csv"



y="""
python_program="python" #Done
link_to_bayesil_app="/home/mkr/bayesil/bayesil-app" #Done
link_to_bayesil_app="%s/Bayesil" % link_to_bayesil_app #Done
link_to_bayesil_program="%s/bayesil.py " % (link_to_bayesil_app) #Done
link_to_job_file="/home/mkr/Documents/Mark/bayesil_web_profile_processing.job"#Done

link_to_setting="/home/mkr/bayesil/bayesil-app/bayesil-app/setting" #Done



link_to_spectrum_dir="%s/%s" % (working_directory,experiment_dir) #Done

Quantify_path="%s" %  working_directory#Done

Quantify_dss_conc=1000.0 

AnnealedParticleMF_nr_samples=10000 

SaveStatus_path="%s" % working_directory

SimplePlot_path="%s/plot.png" % (working_directory)

Save_path="%s" % working_directory

SaveResultAsJson_json_file="%s/result.json" % (working_directory)

LibReading_lib_folder="%s/%s" % (ref_spectra_library_directory,ref_spectra_library_name)

Quantify_lb_file="%s/filters/lower_bound_serum_exceptions.csv" % ref_spectra_library_directory

SaveResultAsJson_confidence_scores_file="%s/filters/confidence_scores.txt" % (ref_spectra_library_directory)

SaveSpectrumXYPairs_path="%s" % working_directory

SaveSpectrumXYPairs_xy_file="%s.txt" % experiment_dir

LibReading_filter_file="%s/filters/serum.txt" % ref_spectra_library_directory

LibReading_weight_list="weightSerum.csv" 




working_directory="/home/mkr/Documents/Mark/serum/test_dir"
experiment_dir="BPAB-40-w0.fid"
ref_spectra_library_name="700"
ref_spectra_library_directory="%s/Libraries" % link_to_bayesil_app #Done
"""





example_of_command="""

python /home/mkr/bayesil/bayesil-app/Bayesil/bayesil.py /home/mkr/Documents/Mark/bayesil_web_profile_processing.job 
--setting /home/mkr/bayesil/bayesil-app/bayesil-app/setting 
--update ReadIOSpectrum.path=/home/mkr/Documents/Mark/serum/test_dir/BPAB-40-w0.fid 
Quantify.path=/home/mkr/Documents/Mark/serum/test_dir 
Quantify.dss_conc=1000.0 
AnnealedParticleMF.nr_samples=10000 
SaveStatus.path=/home/mkr/Documents/Mark/serum/test_dir
SimplePlot.path=/home/mkr/Documents/Mark/serum/test_dir/plot.png 
Save.path=/home/mkr/Documents/Mark/serum/test_dir 
SaveResultAsJson.json_file=/home/mkr/Documents/Mark/serum/test_dir/result.json 
LibReading.lib_folder=/home/mkr/bayesil/bayesil-app/Libraries/700 
Quantify.lb_file=/home/mkr/bayesil/bayesil-app/Libraries/filters/lower_bound_serum_exceptions.csv 
SaveResultAsJson.confidence_scores=/home/mkr/bayesil/bayesil-app/Libraries/filters/confidence_scores.txt 
SaveSpectrumXYPairs.path=/home/mkr/Documents/Mark/serum/test_dir  
SaveSpectrumXYPairs.xy_file=adsasda.txt  
LibReading.filter_file=/home/mkr/bayesil/bayesil-app/Libraries/filters/serum.txt 
LibReading.weight_list=weightSerum.csv 




python /home/mkr/bayesil/bayesil-app/Bayesil/bayesil.py  /home/mkr/Documents/Mark/bayesil_web_profile_processing.job --setting /home/mkr/bayesil/bayesil-app/bayesil-app/setting --update ReadIOSpectrum.path=/home/mkr/Documents/Mark/serum/test_dir/BPAB-40-w0.fid Quantify.path=/home/mkr/Documents/Mark/serum/test_dir Quantify.dss_conc=1000.0 AnnealedParticleMF_nr_samples=10000 SaveStatus.path=/home/mkr/Documents/Mark/serum/test_dir SimplePlot.path=/home/mkr/Documents/Mark/serum/test_dir/plot.png Save.path=/home/mkr/Documents/Mark/serum/test_dir SaveResultAsJson.json_file=/home/mkr/Documents/Mark/serum/test_dir/result.json LibReading.lib_folder=/home/mkr/bayesil/bayesil-app/Libraries/700 Quantify.lb_file=/home/mkr/bayesil/bayesil-app/Libraries/filters/lower_bound_serum_exceptions.csv SaveResultAsJson.confidence_scores=/home/mkr/bayesil/bayesil-app/Libraries/filters/confidence_scores.txt SaveSpectrumXYPairs.xy_file=BPAB-40-w0.fid.txt LibReading.filter_file=/home/mkr/bayesil/bayesil-app/Libraries/filters/serum.txt LibReading.weight_list=weightSerum.csv 

"""




# TMIC Bayesil cluster environment
release_version="20170815225908"
link_to_bayesil_dir="/apps/bayesil/project/releases"
project_subdir="11537"
prject_id="fbb7163ce52dad80e4e7faea4d0b6d74e22d7e13"
dss_concentration="1000.0"



y="""
python 
/apps/bayesil/project/releases/20170815225908/lib/bayesil/app/Bayesil/bayesil.py #DONE
/apps/bayesil/project/releases/20170815225908/lib/bayesil/jobs/bayesil_web_profile.job #DONE 
--setting /apps/bayesil/project/releases/20170815225908/lib/bayesil/app/setting 
--update ReadIOSpectrum.path=/apps/bayesil/project/releases/20170815225908/BayesilWorking/Results/fbb7163ce52dad80e4e7faea4d0b6d74e22d7e13/11537/spectrum.fid 
Quantify.path=/apps/bayesil/project/releases/20170815225908/BayesilWorking/Results/fbb7163ce52dad80e4e7faea4d0b6d74e22d7e13/11537 
Quantify.dss_conc=1000.0 
AnnealedParticleMF.nr_samples=10000 
SaveStatus.path=/apps/bayesil/project/releases/20170815225908/BayesilWorking/Results/fbb7163ce52dad80e4e7faea4d0b6d74e22d7e13/11537 
SimplePlot.path=/apps/bayesil/project/releases/20170815225908/BayesilWorking/Results/fbb7163ce52dad80e4e7faea4d0b6d74e22d7e13/11537/plot.png 
Save.path=/apps/bayesil/project/releases/20170815225908/BayesilWorking/Results/fbb7163ce52dad80e4e7faea4d0b6d74e22d7e13/11537 
SaveResultAsJson.json_file=/apps/bayesil/project/releases/20170815225908/BayesilWorking/Results/fbb7163ce52dad80e4e7faea4d0b6d74e22d7e13/11537/result.json 
LibReading.lib_folder=/apps/bayesil/project/releases/20170815225908/BayesilWorking/Libraries/500 
Quantify.lb_file=/apps/bayesil/project/releases/20170815225908/BayesilWorking/Libraries/filters/lower_bound_serum_exceptions.csv 
SaveResultAsJson.confidence_scores=/apps/bayesil/project/releases/20170815225908/BayesilWorking/Libraries/filters/confidence_scores.txt 
SaveSpectrumXYPairs.path=/apps/bayesil/project/releases/20170815225908/BayesilWorking/Results/fbb7163ce52dad80e4e7faea4d0b6d74e22d7e13/11537 

LibReading.filter_file=/apps/bayesil/project/releases/20170815225908/lib/bayesil/metabolites/serum.txt 

LibReading.weight_list=weightSerum.csv

"""

old="""

link_to_bayesil_program=" %s/%s/lib/bayesil/app/Bayesil/bayesil.py " % (link_to_bayesil_dir,release_version)
link_to_bayesil_web_profile=" %s/%s/lib/bayesil/jobs/bayesil_web_profile.job " % (link_to_bayesil_dir,release_version)
link_to_setting=" --setting  %s/%s/lib/bayesil/app/setting " % (link_to_bayesil_dir,release_version)
if project_subdir!="":
	project_subdir_string="%s/" % project_subdir
	project_subdir_string2="/%s/" % project_subdir

link_to_fid=" -update ReadIOSpectrum.path=%s/%s/BayesilWorking/Results/%s/%sspectrum.fid  " % (link_to_bayesil_dir,release_version, prject_id, project_subdir)
link_to_quantify=" Quantify.path=%s/%s/BayesilWorking/Results/%s%s  " % (link_to_bayesil_dir,release_version, prject_id, project_subdir)
dss_concentration_string=" Quantify.dss_conc=%s " % (dss_concentration)

"""







#######################################################
# Import modules
#######################################################
import sys, os, random, copy
from os import linesep
from os.path import exists
from string import split, upper, lower
from random import shuffle
import time
import datetime
from standard_functions import *
#######################################################


#######################################################
# Pasing arguments
#######################################################

command_text="python "
a=0

if len(sys.argv)>1:
	for item in sys.argv:
		##############################
		# Project/file management 
		##############################
		if item=="-l": 
			location=os.path.abspath(sys.argv[a+1]) 
		elif item=="-p": 
			project=sys.argv[a+1] 
		elif item=="-env": 
			environment=sys.argv[a+1] 
		elif item=="-exp_dir": 
			experiment_dir=sys.argv[a+1] 
		elif item=="-xml_dir": 
			directory_with_xml_files=os.path.abspath(sys.argv[a+1]) 
		a+=1
	command_text+=" %s " % item

#######################################################
# End of pasing arguments
#######################################################




#######################################################
#  Processing arguments
#######################################################							
this_script_full_path=os.path.abspath(sys.argv[0])
this_script_name=os.path.basename(sys.argv[0])
script_location=os.path.dirname(this_script_full_path)
if g_verbose>0:
	print "this_script_full_path=", this_script_full_path
	print "this_script_name=", this_script_name
	print "script_location=", script_location
cur_dir=os.getcwd()
if location=="":
	location=cur_dir
project_dir="%s/%s" % (location,project)

if directory_with_xml_files=="":
	directory_with_xml_files="%s%s" % (project_dir,directory_with_xml_files_default)

#######################################################
#  End of processing arguments
#######################################################


command=""
command+="%s " % python_program # Done
command+="%s " % env_dict[env]["link_to_bayesil_program"] #Done
command+="%s " % env_dict[env]["link_to_job_file"] #Done
command+="%s " % setting_flag # Done
command+="%s " % env_dict[env]["link_to_setting"] #Done
command+="%s " % update_flag #Done
command+="%s" % ReadIOSpectrum_path_argument #Done
command+="%s " % env_dict[env]["link_to_spectrum_dir"] #Done
command+="%s" % Quantify_path_argument #Done
command+="%s " % env_dict[env]["Quantify_path"] #Done
command+="%s" % Quantify_dss_conc_argument #Done
command+="%s " % env_dict[env]["Quantify_dss_conc"] #Done
command+="%s" % AnnealedParticleMF_nr_samples_argument #Done
command+="%s " % env_dict[env]["AnnealedParticleMF_nr_samples"]  # Done
command+="%s" % SaveStatus_path_argument
command+="%s " % env_dict[env]["SaveStatus_path"]  # Done
command+="%s" % SimplePlot_path_argument
command+="%s " % env_dict[env]["SimplePlot_path"]  # Done
command+="%s" % Save_path_argument
command+="%s " %  env_dict[env]["Save_path"]  # Done
command+="%s" % SaveResultAsJson_json_file_argument
command+="%s " % env_dict[env]["SaveResultAsJson_json_file"]  # Done
command+="%s" % LibReading_lib_folder_argument
command+="%s " % env_dict[env]["LibReading_lib_folder"]  # Done
command+="%s" % Quantify_lb_file_argument
command+="%s " % env_dict[env]["Quantify_lb_file"]  # Done
command+="%s" % SaveResultAsJson_confidence_scores_argument
command+="%s " % env_dict[env]["SaveResultAsJson_confidence_scores_file"]  # Done
command+="%s" % SaveSpectrumXYPairs_path_argument
command+="%s " % env_dict[env]["SaveSpectrumXYPairs_path"]  # Done
#command+="%s" % SaveSpectrumXYPairs_xy_file_argument
#command+="%s " % env_dict[env]["SaveSpectrumXYPairs_xy_file"]  # Done
command+="%s" % LibReading_filter_file_argument
command+="%s " % env_dict[env]["LibReading_filter_file"] # Done
command+="%s" % LibReading_weight_list_argument
command+="%s " % env_dict[env]["LibReading_weight_list"] # Done


print "Bayesil_command is "
print command
os.system(command)


xy_spectrum="%s/xy_spectrum_after_processing.txt" % env_dict[env]["project_location"]
xy_spectrum_transposed="%s/xy_spectrum_transposed.txt" % env_dict[env]["project_location"]

print "xy_spectrum_after_processing = ", xy_spectrum


import csv
from itertools import izip
a = izip(*csv.reader(open("%s" % xy_spectrum, "rb")))
csv.writer(open("%s" % xy_spectrum_transposed, "wb")).writerows(a)



