
#######################################################
# Examples of execution
#######################################################

example_of_maple_testing_command="""
#python maple_testing_March_19_2018_A.py -loc /home/mark/Dropbox/Maple -p test_March_19_2018_A -m /home/mark/Dropbox/Maple/March_19_2018
#python maple_testing_March_27_2018_A.py -loc /home/mark/Dropbox/Maple -p test_April_4_2018_A -m /home/mark/Dropbox/Maple/March_27_2018
python maple_testing_April_16_2018_A.py -loc /home/mark/Dropbox/Maple -p test_April_16_2018_A -m /home/mark/Dropbox/Maple/April_16_2018
"""
example_of_maple_command="""
python maple_main_19_march.py  -i /home/mark/Dropbox/Fixing_Bayesil_TMIC_version/tests_at_home/nist_serum_from_tmic_bayesil//xy_before_profiling.txt  -loc /home/mark/Dropbox/Maple/NIST_serum_April_6_2018  -p nist_serum  -b serum  -l /home/mark/Dropbox/Maple/April_05_2018/metabolite_list/serum_new.txt  -w wt_list/serum_weight.xlsx  -dss /home/mark/Dropbox/Maple/April_05_2018/metabolite_list/dss.txt  -s No   -t Yes  -hi /home/mark/Dropbox/Maple/April_05_2018/high_conc/beetle_high_conc.txt  -hc /home/mark/Dropbox/Maple/April_05_2018/high_conc/subtraction_check_list.txt  -cutoff 3000  -m /home/mark/Dropbox/Maple/April_05_2018

python maple_main_19_march.py  -i /home/mark/Dropbox/Fixing_Bayesil_TMIC_version/tests_at_home/nist_serum_from_tmic_bayesil/xy_before_profiling.txt  -loc /home/mark/Dropbox/Maple/test_April_16_2018_A  -p nist_serum  -b serum  -l /home/mark/Dropbox/Maple/April_16_2018/metabolite_list/serum_new.txt  -w wt_list/serum_weight.xlsx   -m /home/mark/Dropbox/Maple/April_16_2018
"""


#######################################################

#######################################################
# Default values
#######################################################
#maple_location="/home/mark/Dropbox/Maple/March_15_2018"
#maple_location="/home/mark/Dropbox/Maple/April_05_2018"
#maple_location="/home/mark/Dropbox/Maple/April_10_2018"
maple_location="/home/mark/Dropbox/Maple/April_16_2018"



#maple_script="maple_main_Mark_branch_March_15_2018_A.py"
#maple_script="maple_main_Mark_branch_March_19_2018_A.py"
maple_script="maple_main_19_march.py"


g_verbose=1
location=""
project="project"
working_dir=""
biofluid="NONE"
library_file=""
wt_list_file="wt_list/serum_weight.xlsx"
constraint_file=""
maple_xml_dir=""
bayesil_xml_dir=""
profile=1
process=0
profile_always=1
make_case_library_files=1
subtraction="No"
tail_corr="No"
cut_off_list="" #[1000, 500,200]
high_conc_metabolites=""
subtraction_check_list_file=""
conc_cutoff=3000
dss_path=""




#######################################################
# End of default values
#######################################################

#######################################################
# Dictionaries
#######################################################
test_dict={}

y="""

wt_list_file = maple_dir + "/wt_list/serum_weight.xlsx"
constrain_file = maple_dir + "/constrain_list/constrain_serum.xlsx"
maple_xml_dir = maple_dir + "/maple_xml_dir/"
bayesil_xml_dir = maple_dir + "/bayesil_xml_dir/"
library_file = maple_dir + "/metabolite_list/serum.txt"
"""



test_dict["test2"]={}
#test_dict["test2"]["txt_spectra_location"]="/home/mark/Dropbox/Fixing_Bayesil_TMIC_version/tests_at_home/nist/"
test_dict["test2"]["txt_spectra_location"]="/home/mark/Dropbox/Fixing_Bayesil_TMIC_version/tests_at_home/nist_serum_from_tmic_bayesil/"
test_dict["test2"]["run"]=1
test_dict["test2"]["wt_list_file"]=""
test_dict["test2"]["constraint_file"]=""
test_dict["test2"]["maple_xml_dir"]=""
test_dict["test2"]["bayesil_xml_dir"]=""
#test_dict["test2"]["library_file"]="/home/mark/Dropbox/Maple/March_15_2018/metabolite_list/serum_reference_spectra.txt"
#test_dict["test2"]["library_file"]="/home/mark/Dropbox/Maple/March_15_2018/metabolite_list/serum.txt"
#test_dict["test2"]["library_file"]="/home/mark/Dropbox/Maple/March_27_2018/metabolite_list/serum_new.txt"
test_dict["test2"]["library_file"]="%s/metabolite_list/serum_new.txt" % maple_location

test_dict["test2"]["process"]=0
test_dict["test2"]["test_name"]="nist_serum"
test_dict["test2"]["biofluid"]="serum"
#test_dict["test2"]["xy_spectrum"]="xy_spectrum_after_processing.txt"
test_dict["test2"]["xy_spectrum"]="xy_before_profiling.txt"
#test_dict["test2"]["xy_spectrum"]="xy_after_profiling.txt"
#test_dict["test2"]["expected_results"]="/home/mark/Dropbox/Maple_Validation/nist_serum_expected_results.csv"
test_dict["test2"]["expected_results"]="/home/mark/Dropbox/Maple_Validation/nist_serum_expected_results_extended.csv"

#test_dict["test2"]["dss_path"]="/home/mark/Dropbox/Maple/March_27_2018/metabolite_list/metabolite_list/dss.txt"
test_dict["test2"]["dss_path"]="/%s/metabolite_list/dss.txt" % maple_location

test_dict["test2"]["subtraction"]="No"
test_dict["test2"]["tail_corr"]=""
test_dict["test2"]["cut_off_list"]=""#[1000, 500,200]

#test_dict["test2"]["high_conc_metabolites"]="/home/mark/Dropbox/Maple/March_27_2018/high_conc/beetle_high_conc.txt"
test_dict["test2"]["high_conc_metabolites"]="%s/high_conc/beetle_high_conc.txt" % maple_location

#test_dict["test2"]["subtraction_check_list_file"]="/home/mark/Dropbox/Maple/March_27_2018//high_conc/subtraction_check_list.txt"
test_dict["test2"]["subtraction_check_list_file"]="%s/high_conc/subtraction_check_list.txt" % maple_location

test_dict["test2"]["conc_cutoff"]=3000
test_dict["test2"]["make_case_library_files"]=1



test_dict["test1"]={}
test_dict["test1"]["txt_spectra_location"]="/home/mark/Dropbox/Testing_Maple/Maple-Lib-Test"
test_dict["test1"]["run"]=0
test_dict["test1"]["wt_list_file"]=""
test_dict["test1"]["constraint_file"]=""
test_dict["test1"]["maple_xml_dir"]=""
test_dict["test1"]["bayesil_xml_dir"]=""
test_dict["test1"]["library_file"]="%s/metabolite_list/serum_reference_spectra.txt" % maple_location
test_dict["test1"]["process"]=0
test_dict["test1"]["biofluid"]="NONE"
test_dict["test1"]["cases"]={}





#y="""
test_dict["test1"]["cases"][1]={}
test_dict["test1"]["cases"][1]["run"]=1
test_dict["test1"]["cases"][1]["wt_list_file"]=""
test_dict["test1"]["cases"][1]["constraint_file"]=""
test_dict["test1"]["cases"][1]["maple_xml_dir"]=""
test_dict["test1"]["cases"][1]["bayesil_xml_dir"]=""
test_dict["test1"]["cases"][1]["library_file"]=""
test_dict["test1"]["cases"][1]["process"]=0
test_dict["test1"]["cases"][1]["biofluid"]="NONE"
test_dict["test1"]["cases"][1]["dss_path"]=""
test_dict["test1"]["cases"][1]["subtraction"]=""
test_dict["test1"]["cases"][1]["tail_corr"]=""
test_dict["test1"]["cases"][1]["cut_off_list"]=""
test_dict["test1"]["cases"][1]["high_conc_metabolites"]=""
test_dict["test1"]["cases"][1]["subtraction_check_list_file"]=""
test_dict["test1"]["cases"][1]["conc_cutoff"]=""




test_dict["test1"]["cases"][1]["txt_spectrum"]="00001.fid.txt"
test_dict["test1"]["cases"][1]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][1]["metabolite_name"]="1-Methylhistidine"
test_dict["test1"]["cases"][1]["hmdb_id"]="HMDB00001"




test_dict["test1"]["cases"][8]={}
test_dict["test1"]["cases"][8]["txt_spectrum"]="00008.fid.txt"
test_dict["test1"]["cases"][8]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][8]["metabolite_name"]="2-Hydroxybutyrate"
test_dict["test1"]["cases"][8]["hmdb_id"]="HMDB00008"


test_dict["test1"]["cases"][42]={}
test_dict["test1"]["cases"][42]["txt_spectrum"]="00042.fid.txt"
test_dict["test1"]["cases"][42]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][42]["metabolite_name"]="Acetic_acid"
test_dict["test1"]["cases"][42]["hmdb_id"]="HMDB00042"


# Results do not exist
test_dict["test1"]["cases"][43]={}
test_dict["test1"]["cases"][43]["txt_spectrum"]="00043.fid.txt"
test_dict["test1"]["cases"][43]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][43]["metabolite_name"]="Betaine"
test_dict["test1"]["cases"][43]["hmdb_id"]="HMDB00043"
test_dict["test1"]["cases"][43]["run"]=0


test_dict["test1"]["cases"][60]={}
test_dict["test1"]["cases"][60]["txt_spectrum"]="00060.fid.txt"
test_dict["test1"]["cases"][60]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][60]["metabolite_name"]="Acetoacetate"
test_dict["test1"]["cases"][60]["hmdb_id"]="HMDB00060"
test_dict["test1"]["cases"][60]["run"]=1

test_dict["test1"]["cases"][62]={}
test_dict["test1"]["cases"][62]["txt_spectrum"]="00062.fid.txt"
test_dict["test1"]["cases"][62]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][62]["metabolite_name"]="L-Carnitine"
test_dict["test1"]["cases"][62]["hmdb_id"]="HMDB00062"


test_dict["test1"]["cases"][64]={}
test_dict["test1"]["cases"][64]["txt_spectrum"]="00064.fid.txt"
test_dict["test1"]["cases"][64]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][64]["metabolite_name"]="Creatine"
test_dict["test1"]["cases"][64]["hmdb_id"]="HMDB00064"


test_dict["test1"]["cases"][94]={}
test_dict["test1"]["cases"][94]["txt_spectrum"]="00094.fid.txt"
test_dict["test1"]["cases"][94]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][94]["metabolite_name"]="Citric_acid"
test_dict["test1"]["cases"][94]["hmdb_id"]="HMDB00094"


test_dict["test1"]["cases"][97]={}
test_dict["test1"]["cases"][97]["txt_spectrum"]="00097.fid.txt"
test_dict["test1"]["cases"][97]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][97]["metabolite_name"]="Choline"
test_dict["test1"]["cases"][97]["hmdb_id"]="HMDB00097"


test_dict["test1"]["cases"][108]={}
test_dict["test1"]["cases"][108]["txt_spectrum"]="00108.fid.txt"
test_dict["test1"]["cases"][108]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][108]["metabolite_name"]="Ethanol"
test_dict["test1"]["cases"][108]["hmdb_id"]="HMDB00108"


test_dict["test1"]["cases"][122]={}
test_dict["test1"]["cases"][122]["txt_spectrum"]="00122.fid.txt"
test_dict["test1"]["cases"][122]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][122]["metabolite_name"]="D-Glucose"
test_dict["test1"]["cases"][122]["hmdb_id"]="HMDB00122"

# Results are not present
test_dict["test1"]["cases"][123]={}
test_dict["test1"]["cases"][123]["txt_spectrum"]="00123.fid.txt"
test_dict["test1"]["cases"][123]["expected_concentration_uM"]=807.4
test_dict["test1"]["cases"][123]["metabolite_name"]="Glycine"
test_dict["test1"]["cases"][123]["hmdb_id"]="HMDB00123"
test_dict["test1"]["cases"][123]["run"]=0

test_dict["test1"]["cases"][131]={}
test_dict["test1"]["cases"][131]["txt_spectrum"]="00131.fid.txt"
test_dict["test1"]["cases"][131]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][131]["metabolite_name"]="Glycerol"
test_dict["test1"]["cases"][131]["hmdb_id"]="HMDB00131"

test_dict["test1"]["cases"][142]={}
test_dict["test1"]["cases"][142]["txt_spectrum"]="00142.fid.txt"
test_dict["test1"]["cases"][142]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][142]["metabolite_name"]="Formic_acid"
test_dict["test1"]["cases"][142]["hmdb_id"]="HMDB00142"

test_dict["test1"]["cases"][148]={}
test_dict["test1"]["cases"][148]["txt_spectrum"]="00148.fid.txt"
test_dict["test1"]["cases"][148]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][148]["metabolite_name"]="L-Glutamic_acid"
test_dict["test1"]["cases"][148]["hmdb_id"]="HMDB00148"

test_dict["test1"]["cases"][157]={}
test_dict["test1"]["cases"][157]["txt_spectrum"]="00157.fid.txt"
test_dict["test1"]["cases"][157]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][157]["metabolite_name"]="Hypoxanthine"
test_dict["test1"]["cases"][157]["hmdb_id"]="HMDB00157"

test_dict["test1"]["cases"][158]={}
test_dict["test1"]["cases"][158]["txt_spectrum"]="00158.fid.txt"
test_dict["test1"]["cases"][158]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][158]["metabolite_name"]="L-Tyrosine"
test_dict["test1"]["cases"][158]["hmdb_id"]="HMDB00158"

test_dict["test1"]["cases"][159]={}
test_dict["test1"]["cases"][159]["txt_spectrum"]="00159.fid.txt"
test_dict["test1"]["cases"][159]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][159]["metabolite_name"]="L-Phenylalanine"
test_dict["test1"]["cases"][159]["hmdb_id"]="HMDB00159"


# Spectrum does not exist
test_dict["test1"]["cases"][161]={}
test_dict["test1"]["cases"][161]["txt_spectrum"]="00161.fid.txt"
test_dict["test1"]["cases"][161]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][161]["metabolite_name"]="L-Alanine"
test_dict["test1"]["cases"][161]["hmdb_id"]="HMDB00161"
test_dict["test1"]["cases"][161]["run"]=0

test_dict["test1"]["cases"][162]={}
test_dict["test1"]["cases"][162]["txt_spectrum"]="00162.fid.txt"
test_dict["test1"]["cases"][162]["expected_concentration_uM"]=17106.0
test_dict["test1"]["cases"][162]["metabolite_name"]="L-Proline"
test_dict["test1"]["cases"][162]["hmdb_id"]="HMDB00162"

test_dict["test1"]["cases"][167]={}
test_dict["test1"]["cases"][167]["txt_spectrum"]="00167.fid.txt"
test_dict["test1"]["cases"][167]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][167]["metabolite_name"]="L-Threonine"
test_dict["test1"]["cases"][167]["hmdb_id"]="HMDB00167"


test_dict["test1"]["cases"][168]={}
test_dict["test1"]["cases"][168]["txt_spectrum"]="00168.fid.txt"
test_dict["test1"]["cases"][168]["expected_concentration_uM"]=1200.0
test_dict["test1"]["cases"][168]["metabolite_name"]="L-Asparagine"
test_dict["test1"]["cases"][168]["hmdb_id"]="HMDB00168"

test_dict["test1"]["cases"][172]={}
test_dict["test1"]["cases"][172]["txt_spectrum"]="00172.fid.txt"
test_dict["test1"]["cases"][172]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][172]["metabolite_name"]="L-Isoleucine"
test_dict["test1"]["cases"][172]["hmdb_id"]="HMDB00172"

test_dict["test1"]["cases"][177]={}
test_dict["test1"]["cases"][177]["txt_spectrum"]="00177.fid.txt"
test_dict["test1"]["cases"][177]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][177]["metabolite_name"]="L-Histidine"
test_dict["test1"]["cases"][177]["hmdb_id"]="HMDB00177"
test_dict["test1"]["cases"][177]["run"]=1

test_dict["test1"]["cases"][182]={}
test_dict["test1"]["cases"][182]["txt_spectrum"]="00182.fid.txt"
test_dict["test1"]["cases"][182]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][182]["metabolite_name"]="Lysine"
test_dict["test1"]["cases"][182]["hmdb_id"]="HMDB00182"
test_dict["test1"]["cases"][182]["run"]=1

test_dict["test1"]["cases"][187]={}
test_dict["test1"]["cases"][187]["txt_spectrum"]="00187.fid.txt"
test_dict["test1"]["cases"][187]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][187]["metabolite_name"]="L-Serine"
test_dict["test1"]["cases"][187]["hmdb_id"]="HMDB00187"
test_dict["test1"]["cases"][187]["run"]=1

test_dict["test1"]["cases"][190]={}
test_dict["test1"]["cases"][190]["txt_spectrum"]="00190.fid.txt"
test_dict["test1"]["cases"][190]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][190]["metabolite_name"]="L-Lactic_acid"
test_dict["test1"]["cases"][190]["hmdb_id"]="HMDB00190"

test_dict["test1"]["cases"][191]={}
test_dict["test1"]["cases"][191]["txt_spectrum"]="00191.fid.txt"
test_dict["test1"]["cases"][191]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][191]["metabolite_name"]="L-Aspartic_acid"
test_dict["test1"]["cases"][191]["hmdb_id"]="HMDB00191"

test_dict["test1"]["cases"][214]={}
test_dict["test1"]["cases"][214]["txt_spectrum"]="00214.fid.txt"
test_dict["test1"]["cases"][214]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][214]["metabolite_name"]="Ornithine"
test_dict["test1"]["cases"][214]["hmdb_id"]="HMDB00214"

test_dict["test1"]["cases"][243]={}
test_dict["test1"]["cases"][243]["txt_spectrum"]="00243.fid.txt"
test_dict["test1"]["cases"][243]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][243]["metabolite_name"]="Pyruvic_acid"
test_dict["test1"]["cases"][243]["hmdb_id"]="HMDB00243"

test_dict["test1"]["cases"][254]={}
test_dict["test1"]["cases"][254]["txt_spectrum"]="00254.fid.txt"
test_dict["test1"]["cases"][254]["expected_concentration_uM"]=730.0
test_dict["test1"]["cases"][254]["metabolite_name"]="Succinic_acid"
test_dict["test1"]["cases"][254]["hmdb_id"]="HMDB00254"

test_dict["test1"]["cases"][357]={}
test_dict["test1"]["cases"][357]["txt_spectrum"]="00357.fid.txt"
test_dict["test1"]["cases"][357]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][357]["metabolite_name"]="3-Hydroxybutyric_acid"
test_dict["test1"]["cases"][357]["hmdb_id"]="HMDB00357"

test_dict["test1"]["cases"][517]={}
test_dict["test1"]["cases"][517]["txt_spectrum"]="00517.fid.txt"
test_dict["test1"]["cases"][517]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][517]["metabolite_name"]="L-Arginine"
test_dict["test1"]["cases"][517]["hmdb_id"]="HMDB00517"

test_dict["test1"]["cases"][562]={}
test_dict["test1"]["cases"][562]["txt_spectrum"]="00562.fid.txt"
test_dict["test1"]["cases"][562]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][562]["metabolite_name"]="Creatinine"
test_dict["test1"]["cases"][562]["hmdb_id"]="HMDB00562"

test_dict["test1"]["cases"][641]={}
test_dict["test1"]["cases"][641]["txt_spectrum"]="00641.fid.txt"
test_dict["test1"]["cases"][641]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][641]["metabolite_name"]="L-Glutamine"
test_dict["test1"]["cases"][641]["hmdb_id"]="HMDB00641"

test_dict["test1"]["cases"][687]={}
test_dict["test1"]["cases"][687]["txt_spectrum"]="00687.fid.txt"
test_dict["test1"]["cases"][687]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][687]["metabolite_name"]="L-Leucine"
test_dict["test1"]["cases"][687]["hmdb_id"]="HMDB00687"

test_dict["test1"]["cases"][691]={}
test_dict["test1"]["cases"][691]["txt_spectrum"]="00691.fid.txt"
test_dict["test1"]["cases"][691]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][691]["metabolite_name"]="Malonic_acid"
test_dict["test1"]["cases"][691]["hmdb_id"]="HMDB00691"

test_dict["test1"]["cases"][696]={}
test_dict["test1"]["cases"][696]["txt_spectrum"]="00696.fid.txt"
test_dict["test1"]["cases"][696]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][696]["metabolite_name"]="L-Methionine"
test_dict["test1"]["cases"][696]["hmdb_id"]="HMDB00696"


test_dict["test1"]["cases"][863]={}
test_dict["test1"]["cases"][863]["txt_spectrum"]="00863.fid.txt"
test_dict["test1"]["cases"][863]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][863]["metabolite_name"]="Isopropanol"
test_dict["test1"]["cases"][863]["hmdb_id"]="HMDB00863"

test_dict["test1"]["cases"][883]={}
test_dict["test1"]["cases"][883]["txt_spectrum"]="00883.fid.txt"
test_dict["test1"]["cases"][883]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][883]["metabolite_name"]="L-Valine"
test_dict["test1"]["cases"][883]["hmdb_id"]="HMDB00883"


test_dict["test1"]["cases"][929]={}
test_dict["test1"]["cases"][929]["txt_spectrum"]="00929.fid.txt"
test_dict["test1"]["cases"][929]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][929]["metabolite_name"]="L-Tryptophan"
test_dict["test1"]["cases"][929]["hmdb_id"]="HMDB00929"

test_dict["test1"]["cases"][1659]={}
test_dict["test1"]["cases"][1659]["txt_spectrum"]="01659.fid.txt"
test_dict["test1"]["cases"][1659]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][1659]["metabolite_name"]="Acetone"
test_dict["test1"]["cases"][1659]["hmdb_id"]="HMDB01659"
test_dict["test1"]["cases"][1659]["run"]=1

test_dict["test1"]["cases"][1873]={}
test_dict["test1"]["cases"][1873]["txt_spectrum"]="01873.fid.txt"
test_dict["test1"]["cases"][1873]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][1873]["metabolite_name"]="Isobutyric_acid"
test_dict["test1"]["cases"][1873]["hmdb_id"]="HMDB01873"
test_dict["test1"]["cases"][1873]["run"]=1

test_dict["test1"]["cases"][1875]={}
test_dict["test1"]["cases"][1875]["txt_spectrum"]="01875.fid.txt"
test_dict["test1"]["cases"][1875]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][1875]["metabolite_name"]="Methanol"
test_dict["test1"]["cases"][1875]["hmdb_id"]="HMDB01875"

test_dict["test1"]["cases"][1881]={}
test_dict["test1"]["cases"][1881]["txt_spectrum"]="01881.fid.txt"
test_dict["test1"]["cases"][1881]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][1881]["metabolite_name"]="Propylene_glycol"
test_dict["test1"]["cases"][1881]["hmdb_id"]="HMDB01881"

test_dict["test1"]["cases"][4983]={}
test_dict["test1"]["cases"][4983]["txt_spectrum"]="04983.fid.txt"
test_dict["test1"]["cases"][4983]["expected_concentration_uM"]=1000.0
test_dict["test1"]["cases"][4983]["metabolite_name"]="Dimethylsulfone"
test_dict["test1"]["cases"][4983]["hmdb_id"]="HMDB04983"
test_dict["test1"]["cases"][4983]["run"]=1
#"""


#######################################################
# End of dictionaries
#######################################################

#######################################################
# Import modules
#######################################################
import sys, os, random, copy, shutil, xmltodict, json, math
from os import linesep
from os.path import exists
from string import split, upper, lower
from random import shuffle
import time
import datetime
from standard_functions import *
#######################################################
# End of importing modules
#######################################################



#######################################################
# Functions
#######################################################
def main(\
    project_dir,\
    test_dict,\
    maple_location,\
    maple_script,\
    biofluid,\
    library_file,\
    wt_list_file,\
    constraint_file,\
    maple_xml_dir,\
    bayesil_xml_dir,\
    profile,\
    process,\
    profile_always,\
    g_make_case_library_files,\
    project,\
    subtraction,\
    tail_corr,\
    cut_off_list,\
    high_conc_metabolites,\
    subtraction_check_list_file,\
    conc_cutoff,\
    dss_path,\
    ):
    """
    Main function


test_dict["test1"]={}
test_dict["test1"]["txt_spectra_location"]="/home/mark/Dropbox/Testing_Maple/Maple-Lib-Test"
test_dict["test1"]["run"]=1

test_dict["test1"]["cases"]={}
test_dict["test1"]["cases"]["1"]={}
test_dict["test1"]["cases"]["1"]["run"]=1
test_dict["test1"]["cases"]["1"]["txt_spectrum"]="00008.fid.txt"
test_dict["test1"]["cases"]["1"]["expected_concentration_uM"]=1000
test_dict["test1"]["cases"]["1"]["metabolite_name"]="2-Hydroxybutyrate"
test_dict["test1"]["cases"]["1"]["hmdb_id"]="HMDB00008"


    """
    verbose=1
    if verbose>=1 or g_verbose>=1: 
        print "Running function main()"    
        print "maple_location = ",maple_location
        print "maple_script = ",maple_script
        print "project_dir = ", project_dir


    debug=0
    if debug==1:
        print "Exiting..."
        sys.exit()


    make_dir_no_remove(project_dir)
        

    for test in test_dict:
        if verbose>=1 or g_verbose>=1: 
            print "test = ", test

        run_test=test_dict[test]["run"]

        txt_spectra_location=""
        if "txt_spectra_location" in test_dict[test]:
            txt_spectra_location=test_dict[test]["txt_spectra_location"]

        test_wt_list_file=""
        test_constraint_file=""
        test_maple_xml_dir=""
        test_bayesil_xml_dir=""
        test_library_file=""
        test_process=""
        test_biofluid=""
        test_dss_path=""
        tail_corr_path=""
        test_tail_corr=""
        test_cut_off_list=""
        test_subtraction=""
        test_subtraction_check_list_file=""
        test_conc_cutoff=""
        test_high_conc_metabolites=""


        case_wt_list_file=""
        case_constraint_file=""
        case_maple_xml_dir=""
        case_bayesil_xml_dir=""
        case_library_file=""
        case_process=""
        case_biofluid=""
        case_dss_path=""
        case_corr_path=""
        case_tail_corr=""
        case_cut_off_list=""
        case_subtraction=""
        case_subtraction_check_list_file=""
        case_conc_cutoff=""
        case_high_conc_metabolites=""
   
        test_name=test
        if "test_name" in test_dict[test]:
            test_name=test_dict[test]["test_name"]


        if "wt_list_file" in test_dict[test]:
            test_wt_list_file=test_dict[test]["wt_list_file"]

        if "constraint_file" in test_dict[test]:
            test_constraint_file=test_dict[test]["constraint_file"]


        if "maple_xml_dir" in test_dict[test]:
            test_maple_xml_dir=test_dict[test]["maple_xml_dir"]


        if "bayesil_xml_dir" in test_dict[test]:
            test_bayesil_xml_dir=test_dict[test]["bayesil_xml_dir"]

        if "library_file" in test_dict[test]:
            test_library_file=test_dict[test]["library_file"]

        if "process" in test_dict[test]:
            test_process=test_dict[test]["process"]

        if "biofluid" in test_dict[test]:
            test_biofluid=test_dict[test]["biofluid"]

        if "make_case_library_files" in test_dict[test]:
            make_case_library_files=test_dict[test]["make_case_library_files"]
        else:
            make_case_library_files=g_make_case_library_files

        if "dss_path" in test_dict[test]:
            test_dss_path=test_dict[test]["dss_path"]

        if "tail_corr" in test_dict[test]:
            test_tail_corr=test_dict[test]["tail_corr"]

        if "cut_off_list" in test_dict[test]:
            test_cut_off_list=test_dict[test]["cut_off_list"]

        if "subtraction" in test_dict[test]:
            test_subtraction=test_dict[test]["subtraction"]

        if "high_conc_metabolites" in test_dict[test]:
            test_high_conc_metabolites=test_dict[test]["high_conc_metabolites"]

        if "subtraction_check_list_file" in test_dict[test]:
           test_subtraction_check_list_file=test_dict[test]["subtraction_check_list_file"]

        if "conc_cutoff" in test_dict[test]:
           test_conc_cutoff=test_dict[test]["conc_cutoff"]



        if run_test==1 and txt_spectra_location!="":

            if "cases" not in test_dict[test]:
                expected_results=""
                xy_spectrum=""
                if "xy_spectrum" in test_dict[test]:
                    xy_spectrum=test_dict[test]["xy_spectrum"] 
                if "expected_results" in test_dict[test]:
                    expected_results=test_dict[test]["expected_results"]


                if xy_spectrum!="":
                    run_maple(\
                        test_name,\
                        project_dir,\
                        maple_location,\
                        maple_script,\
                        txt_spectra_location,\
                        xy_spectrum,\
                        test_wt_list_file,\
                        test_constraint_file,\
                        test_maple_xml_dir,\
                        test_bayesil_xml_dir,\
                        test_library_file,\
                        test_process,\
                        test_biofluid,\
                        case_wt_list_file,\
                        case_constraint_file,\
                        case_maple_xml_dir,\
                        case_bayesil_xml_dir,\
                        case_library_file,\
                        case_process,\
                        case_biofluid,\
                        biofluid,\
                        library_file,\
                        wt_list_file,\
                        constraint_file,\
                        maple_xml_dir,\
                        bayesil_xml_dir,\
                        profile,\
                        process,\
                        case_dss_path,\
                        test_dss_path,\
                        dss_path,\
                        case_tail_corr,\
                        test_tail_corr,\
                        tail_corr,\
                        case_cut_off_list,\
                        test_cut_off_list,\
                        cut_off_list,\
                        case_subtraction,\
                        test_subtraction,\
                        subtraction,\
                        case_high_conc_metabolites,\
                        test_high_conc_metabolites,\
                        high_conc_metabolites,\
                        case_subtraction_check_list_file,\
                        test_subtraction_check_list_file,\
                        subtraction_check_list_file,\
                        case_conc_cutoff,\
                        test_conc_cutoff,\
                        conc_cutoff,\
                        )

                    concentration_dict=parse_maple_output_all(\
                        test_name,\
                        project_dir,\
                        )

                    expected_results_dict={}
                    report_text=""
                    missing_result_text=""
                    if expected_results!="":
                        expected_results_dict=parse_expected_results_file(expected_results)

                        report_text, missing_result_text=compare_results_with_expected_values(concentration_dict,expected_results_dict)


                    write_report(\
                        test_dict,\
                        project_dir,\
                        report_text,\
                        project,\
                        )


                    missing_result_file="%s/%s_missing_results.csv" % (project_dir,project)
                    print "Writing  missing_result_text to ", missing_result_file
                    write_file(missing_result_file,missing_result_text)


                    y="""
'


                        diff_percent="NONE"
                        concentration=concentration.replace("\n","")
                        concentration=concentration.replace("\r","")
                        if is_number(concentration)==1:
                            diff_percent=(abs( float(concentration) - expected_concentration_uM)/expected_concentration_uM)*100.0
                        #print "diff_percent = ", diff_percent
                        #sys.exit()

                        test_dict[test]["cases"][case]["concentration"]=concentration
                        test_dict[test]["cases"][case]["diff_percent"]=diff_percent
                        report_text+="%s,%s,%s,%s,%s" % (hmdb_id,metabolite_name, expected_concentration_uM, concentration, diff_percent) + linesep



                write_report(\
                        test_dict,\
                        project_dir,\
                        report_text,\
                        project,\
                        )
                    """


                #test_dict["test2"]["xy_spectrum"]="xy_spectrum_transposed.txt"
                #test_dict["test2"]["expected_results"]="/home/mark/Dropbox/Fixing_Bayesil_TMIC_version/tests_at_home/nist/expected_values.csv"



            #if "cases" in test_dict[test]:
            else:
                case_list=[]
                for case in test_dict[test]["cases"]:
                    case_list+=[case]           
                #print "case_list before sorting ", case_list

                case_list.sort()

                #print "case_list after sorting ", case_list
                #print "Exiting..."
                #sys.exit()


                report_text="hmdb_id,metabolite_name,expected_concentration_uM,concentration diff_percent" + linesep

                hmdb_list=[]
                name_list=[]

                #for case in test_dict[test]["cases"]:
                for case in case_list:

                    run_case=1
                    if "run" in test_dict[test]["cases"][case]:
                        run_case=test_dict[test]["cases"][case]["run"]

                    



                    txt_spectrum=test_dict[test]["cases"][case]["txt_spectrum"]
                    expected_concentration_uM=test_dict[test]["cases"][case]["expected_concentration_uM"]
                    metabolite_name=test_dict[test]["cases"][case]["metabolite_name"]
                    hmdb_id=test_dict[test]["cases"][case]["hmdb_id"]

                    metabolite=metabolite_name.replace(" ","_")
                    case_name="%s_%s_%s" % (case, hmdb_id,metabolite)

                    if run_case==1:

                        if hmdb_id not in hmdb_list:                        
                            hmdb_list+=[hmdb_id]
                        else:
                            print "Error!!! HMDB ID %s is already in hmdb_list" % hmdb_id
                            print "The program  will exit now"
                            sys.exit()

                        if metabolite_name not in name_list:                        
                            name_list+=[metabolite_name]
                        else:
                            print "Error!!! metabolite_name %s is already in name_list" % metabolite_name
                            print "The program  will exit now"
                            sys.exit()




                        make_dir_no_remove(project_dir)                        
                        case_dir="%s/%s" % (project_dir,case_name)
                        make_dir_no_remove(case_dir)    
                        result_dir="%s/Results" % (case_dir)
                        make_dir_no_remove(result_dir)    
                        result_file="%s/out_result_all_conc.csv" % (result_dir)
                        case_wt_list_file=""
                        case_constraint_file=""
                        case_maple_xml_dir=""
                        case_bayesil_xml_dir=""
                        case_library_file=""
                        case_process=""
                        case_biofluid=""
                        case_dss_path=""
                        case_corr_path=""
                        case_tail_corr=""
                        case_cut_off_list=""
                        case_subtraction=""
                        case_high_conc_metabolites=""
                        case_subtraction_check_list_file=""
                        case_conc_cutoff=""

                        if "wt_list_file" in test_dict[test]["cases"][case]:
                            case_wt_list_file=test_dict[test]["cases"][case]["wt_list_file"]

                        if "constraint_file" in test_dict[test]["cases"][case]:
                            case_constraint_file=test_dict[test]["cases"][case]["constraint_file"]


                        if "maple_xml_dir" in test_dict[test]["cases"][case]:
                            case_maple_xml_dir=test_dict[test]["cases"][case]["maple_xml_dir"]


                        if "bayesil_xml_dir" in test_dict[test]["cases"][case]:
                            case_bayesil_xml_dir=test_dict[test]["cases"][case]["bayesil_xml_dir"]

                        if "library_file" in test_dict[test]["cases"][case]:
                            case_library_file=test_dict[test]["cases"][case]["library_file"]


                        if "process" in test_dict[test]["cases"][case]:
                            case_process=test_dict[test]["cases"][case]["process"]

                        if "biofluid" in test_dict[test]["cases"][case]:
                            case_biofluid=test_dict[test]["cases"][case]["biofluid"]

                        if make_case_library_files==1:
                            case_library_file=make_case_library_file(hmdb_id,result_dir)

                        if "dss_path" in test_dict[test]["cases"][case]:
                            case_dss_path=test_dict[test]["cases"][case]["dss_path"]

                        if "tail_corr" in test_dict[test]["cases"][case]:
                            case_tail_corr=test_dict[test]["cases"][case]["tail_corr"]

                        if "cut_off_list" in test_dict[test]["cases"][case]:
                            case_cut_off_list=test_dict[test]["cases"][case]["cut_off_list"]

                        if "subtraction" in test_dict[test]["cases"][case]:
                            case_subtraction=test_dict[test]["cases"][case]["subtraction"]

                        if "case_subtraction" in test_dict[test]["cases"][case]:
                            case_subtraction=test_dict[test]["cases"][case]["subtraction"]

                        if "high_conc_metabolites" in test_dict[test]["cases"][case]:
                            case_high_conc_metabolites=test_dict[test]["cases"][case]["high_conc_metabolites"]

                        if "subtraction_check_list_file" in test_dict[test]["cases"][case]:
                            case_subtraction_check_list_file=test_dict[test]["cases"][case]["subtraction_check_list_file"]

                        if "conc_cutoff" in test_dict[test]["cases"][case]:
                            case_conc_cutoff=test_dict[test]["cases"][case]["conc_cutoff"]

                        if verbose>=1 or g_verbose>=1: 
                            print "test_case = ", case
                            print "txt_spectrum = ", txt_spectrum
                            print "expected_concentration_uM = ",expected_concentration_uM
                            print "metabolite_name = ",metabolite_name
                            print "hmdb_id = ",hmdb_id

                            print "test_wt_list_file = ", test_wt_list_file
                            print "test_constraint_file = ", test_constraint_file
                            print "test_maple_xml_dir = ",test_maple_xml_dir
                            print "test_bayesil_xml_dir = ",test_bayesil_xml_dir
                            print "test_library_file = ",test_library_file
                            print "test_process = ", test_process
                            print "test_biofluid = ", test_biofluid



                            print "case_wt_list_file = ", case_wt_list_file
                            print "case_constraint_file = ", case_constraint_file
                            print "case_maple_xml_dir = ",case_maple_xml_dir
                            print "case_bayesil_xml_dir = ",case_bayesil_xml_dir
                            print "case_library_file = ",case_library_file
                            print "case_process = ", test_process
                            print "case_biofluid = ", test_biofluid

                        if file_exists(result_file)==0 or profile_always==1:


                            run_maple(\
                        case_name,\
                        project_dir,\
                        maple_location,\
                        maple_script,\
                        txt_spectra_location,\
                        txt_spectrum,\
                        test_wt_list_file,\
                        test_constraint_file,\
                        test_maple_xml_dir,\
                        test_bayesil_xml_dir,\
                        test_library_file,\
                        test_process,\
                        test_biofluid,\
                        case_wt_list_file,\
                        case_constraint_file,\
                        case_maple_xml_dir,\
                        case_bayesil_xml_dir,\
                        case_library_file,\
                        case_process,\
                        case_biofluid,\
                        biofluid,\
                        library_file,\
                        wt_list_file,\
                        constraint_file,\
                        maple_xml_dir,\
                        bayesil_xml_dir,\
                        profile,\
                        process,\
                        case_dss_path,\
                        test_dss_path,\
                        dss_path,\
                        case_tail_corr,\
                        test_tail_corr,\
                        tail_corr,\
                        case_cut_off_list,\
                        test_cut_off_list,\
                        cut_off_list,\
                        case_subtraction,\
                        test_subtraction,\
                        subtraction,\
                        case_high_conc_metabolites,\
                        test_high_conc_metabolites,\
                        high_conc_metabolites,\
                        case_subtraction_check_list_file,\
                        test_subtraction_check_list_file,\
                        subtraction_check_list_file,\
                        case_conc_cutoff,\
                        test_conc_cutoff,\
                        conc_cutoff,\
                        )

                        concentration=parse_maple_output(\
                        case_name,\
                        project_dir,\
                        hmdb_id,\
                        metabolite_name,\
                        )

                        diff_percent="NONE"
                        concentration=concentration.replace("\n","")
                        concentration=concentration.replace("\r","")
                        if is_number(concentration)==1:
                            diff_percent=(abs( float(concentration) - expected_concentration_uM)/expected_concentration_uM)*100.0
                        #print "diff_percent = ", diff_percent
                        #sys.exit()

                        test_dict[test]["cases"][case]["concentration"]=concentration
                        test_dict[test]["cases"][case]["diff_percent"]=diff_percent
                        report_text+="%s,%s,%s,%s,%s" % (hmdb_id,metabolite_name, expected_concentration_uM, concentration, diff_percent) + linesep



                write_report(\
                        test_dict,\
                        project_dir,\
                        report_text,\
                        project,\
                        )
             
    debug=1
    if debug==1:
        print "Exiting after function main()"
        sys.exit()
    return



def compare_results_with_expected_values(concentration_dict,expected_results_dict):
    """
    Function to compare results with expected values
                concentration_dict[hmdb_id]={"metabolite_name":metabolite_name, "concentration":float(concentration)}

                        if is_number(concentration)==1:
                            diff_percent=(abs( float(concentration) - expected_concentration_uM)/expected_concentration_uM)*100.0
                        #print "diff_percent = ", diff_percent
                        #sys.exit()

                        test_dict[test]["cases"][case]["concentration"]=concentration
                        test_dict[test]["cases"][case]["diff_percent"]=diff_percent
                        report_text+="%s,%s,%s,%s,%s" % (hmdb_id,metabolite_name, expected_concentration_uM, concentration, diff_percent) + linesep


    """
    verbose=0
    if verbose>=1 or g_verbose>=1: 
        print "Running function compare_results_with_expected_values()"


    l_text=""
    l_missing_results=""

    for hmdb_id in concentration_dict:
        metabolite_name=concentration_dict[hmdb_id]["metabolite_name"] 
        concentration=concentration_dict[hmdb_id]["concentration"] 
        diff_percent="NONE"    
        if hmdb_id not in expected_results_dict:
            print "Warning!!! expected_results_dict does not have hmdb_id ", hmdb_id 
            l_missing_results+="Expected_results_dict does not have hmdb_id %s " % hmdb_id  + linesep
        else:
            exp_metabolite_name=expected_results_dict[hmdb_id]["metabolite_name"] 
            exp_concentration=expected_results_dict[hmdb_id]["concentration"]

            if exp_metabolite_name!=metabolite_name:
                print "Warning!!! Metabolite %s with HMDB ID %s has different name %s in expected_results_dict" % (metabolite_name,hmdb_id,exp_metabolite_name)   

            if is_number(concentration)==0:
                print "Warning!!! Concentration of Metabolite %s with HMDB ID %s in concentration_dict is not numerical and is " % (metabolite_name,hmdb_id), [concentration]

            if is_number(exp_concentration)==0:
                print "Warning!!! Concentration of Metabolite %s with HMDB ID %s in expected_results_dict is not numerical and is " % (metabolite_name,hmdb_id), [exp_concentration]

            if is_number(concentration)==1 and is_number(exp_concentration)==1 and exp_concentration!=0:
                diff_percent="%.2f" % float((abs( float(concentration) - exp_concentration)/exp_concentration)*100.0)

            concentration_dict[hmdb_id]["diff_percent"]=diff_percent            
            l_text+="%s,%s,%s,%s,%s" % (hmdb_id ,metabolite_name,concentration,exp_concentration,diff_percent) + linesep

    for hmdb_id in expected_results_dict:
        exp_metabolite_name=expected_results_dict[hmdb_id]["metabolite_name"] 
        exp_concentration=expected_results_dict[hmdb_id]["concentration"]
        if hmdb_id not in concentration_dict:
            print "Warning!!! concentration_dict does not have hmdb_id ", hmdb_id 
            l_missing_results+="Concentration_dict does not have hmdb_id %s " % hmdb_id  + linesep
            l_text+="%s,%s,NONE,%s,NONE" % (hmdb_id ,exp_metabolite_name,exp_concentration) + linesep
            concentration_dict[hmdb_id]={}
            concentration_dict[hmdb_id]["diff_percent"]="NONE"           
            concentration_dict[hmdb_id]["concentration"]="NONE"
            concentration_dict[hmdb_id]["metabolite_name"]=exp_metabolite_name                                   

    debug=0
    if debug==1:
        print "concentration_dict = ", concentration_dict
        print "l_text = ", l_text
        print "Exiting after compare_results_with_expected_values()"
        sys.exit()
    return l_text,l_missing_results



def parse_expected_results_file(expected_results):
    """
    Parse expected results file
i =  HMDB04983,Dimethylsulfone,4.5

    """
    verbose=0
    if verbose>=1 or g_verbose>=1: 
        print "Running function parse_expected_results_file()" 

    file_lines=read_file(expected_results)
    concentration_dict={}
    for i in file_lines:
        i=i.replace("\r\n","")
        i=i.replace("\n","")
        i_split=i.split(",")
        if len(i_split)<3:
            print "Error!!! The length of i_split < 3", [i_split]
        else:
            concentration= i_split[2]
            hmdb_id=i_split[0]
            metabolite_name=i_split[1]
            if is_number(concentration):
                concentration_dict[hmdb_id]={"metabolite_name":metabolite_name, "concentration":float(concentration)}
            else:
                print "Warning!!!!  concentration %s is not numeric for %s %s" % (concentration,hmdb_id,metabolite_name)  
    debug=0
    if debug==1:
        print "concentration_dict = ", concentration_dict
        print "Exiting after parse_expected_results_file()"
        sys.exit()
    return concentration_dict


def make_case_library_file(\
                    hmdb_id,\
                    result_dir\
                    ):
    """
    Function to make library file for a case
    """
    verbose=0
    if verbose>=1 or g_verbose>=1: 
        print "Running function make_case_library_file()" 
        print "hmdb_id = ", hmdb_id
        print "result_dir = ", result_dir

    #make_dir_no_remove(result_dir)

    l_text="HMDB00000"+linesep
    l_text+="%s" % hmdb_id +linesep
    
    l_file="%s/%s.lib" % (result_dir, hmdb_id)

    write_file(l_file,l_text)

    debug=0
    if debug==1:
        print "l_file = ", l_file
        print "Exiting after function make_case_library_file()"
        sys.exit()
    return l_file

def run_maple(\
            case,\
            project_dir,\
            maple_location,\
            maple_script,\
            txt_spectra_location,\
            txt_spectrum,\
            test_wt_list_file,\
            test_constraint_file,\
            test_maple_xml_dir,\
            test_bayesil_xml_dir,\
            test_library_file,\
            test_process,\
            test_biofluid,\
            case_wt_list_file,\
            case_constraint_file,\
            case_maple_xml_dir,\
            case_bayesil_xml_dir,\
            case_library_file,\
            case_process,\
            case_biofluid,\
            biofluid,\
            library_file,\
            wt_list_file,\
            constraint_file,\
            maple_xml_dir,\
            bayesil_xml_dir,\
            profile,\
            process,\
            case_dss_path,\
            test_dss_path,\
            dss_path,\
            case_tail_corr,\
            test_tail_corr,\
            tail_corr,\
            case_cut_off_list,\
            test_cut_off_list,\
            cut_off_list,\
            case_subtraction,\
            test_subtraction,\
            subtraction,\
            case_high_conc_metabolites,\
            test_high_conc_metabolites,\
            high_conc_metabolites,\
            case_subtraction_check_list_file,\
            test_subtraction_check_list_file,\
            subtraction_check_list_file,\
            case_conc_cutoff,\
            test_conc_cutoff,\
            conc_cutoff,\
              ):
    """
    Function to run Maple

    python maple_main_Mark_branch_March_15_2018_A.py  -i data/B60.fid.txt -w wt_list/serum_weight.xlsx -m /home/mark/Dropbox/Maple/March_15_2018

    """
    verbose=1
    if verbose>=1 or g_verbose>=1: 
        print "Running function run_maple()"    
        print "maple_location = ",maple_location
        print "maple_script = ",maple_script
        print "txt_spectra_location = ", txt_spectra_location
        print "txt_spectrum = ", txt_spectrum
        #print "metabolite_name = ", metabolite_name
        #print "hmdb_id = ", hmdb_id
        print "test_wt_list_file = ", test_wt_list_file
        print "test_constraint_file = ", test_constraint_file
        print "test_maple_xml_dir = ",test_maple_xml_dir
        print "test_bayesil_xml_dir = ",test_bayesil_xml_dir
        print "test_library_file = ",test_library_file
        print "test_process = ", test_process
        print "test_biofluid = ", test_biofluid

        print "test_dss_path = ", test_dss_path
        print "test_tail_corr = ", test_tail_corr
        print "test_high_conc_metabolites = ", test_high_conc_metabolites
        print "test_subtraction_check_list_file = ", test_subtraction_check_list_file
        print "test_conc_cutoff = ", test_conc_cutoff

        print "case_wt_list_file = ", case_wt_list_file
        print "case_constraint_file = ", case_constraint_file
        print "case_maple_xml_dir = ",case_maple_xml_dir
        print "case_bayesil_xml_dir = ",case_bayesil_xml_dir
        print "case_library_file = ",case_library_file
        print "case_process = ", test_process
        print "case_biofluid = ", case_biofluid

        print "case_dss_path = ", case_dss_path
        print "case_tail_corr = ", case_tail_corr
        print "case_high_conc_metabolites = ", case_high_conc_metabolites
        print "case_subtraction_check_list_file = ", case_subtraction_check_list_file
        print "case_conc_cutoff = ", case_conc_cutoff

    txt_spectra_location_full_path="%s/%s" % (txt_spectra_location,txt_spectrum)

    if file_exists(txt_spectra_location_full_path)==0:
        print "Error!!! File with results %s does not exist" % txt_spectra_location_full_path
        print "Program will exit now"
        sys.exit()

    cur_dir=os.getcwd()
    os.chdir(maple_location)
    
    maple_command="python %s " % maple_script
    maple_command+=" -i %s " %  txt_spectra_location_full_path
    maple_command+=" -loc %s " %  project_dir
    maple_command+=" -p %s " %  case


    if case_biofluid not in ["NONE",""]:
        maple_command+=" -b %s " %  case_biofluid
    elif test_biofluid not in ["NONE",""]:
        maple_command+=" -b %s " %  test_biofluid
    elif biofluid not in ["NONE",""]:
        maple_command+=" -b %s " %  biofluid   


    if case_constraint_file !="":
        maple_command+=" -c %s " %  case_constraint_file
    elif test_constraint_file !="":
        maple_command+=" -c %s " %  test_constraint_file
    elif constraint_file !="":
        maple_command+=" -c %s " %  constraint_file


    if case_library_file !="":
        maple_command+=" -l %s " %  case_library_file
    elif test_library_file !="":
        maple_command+=" -l %s " %  test_library_file
    elif library_file !="":
        maple_command+=" -l %s " %  library_file


    if case_wt_list_file !="":
        maple_command+=" -w %s " %  case_wt_list_file
    elif test_wt_list_file !="":
        maple_command+=" -w %s " %  test_wt_list_file
    elif wt_list_file !="":
        maple_command+=" -w %s " %  wt_list_file


    if case_maple_xml_dir !="":
        maple_command+=" -m_xml %s " %  case_maple_xml_dir
    elif test_maple_xml_dir !="":
        maple_command+=" -m_xml %s " %  test_maple_xml_dir
    elif maple_xml_dir !="":
        maple_command+=" -m_xml %s " %  maple_xml_dir

    if case_bayesil_xml_dir !="":
        maple_command+=" -b_xml %s " %  case_bayesil_xml_dir
    elif test_bayesil_xml_dir !="":
        maple_command+=" -b_xml %s " %  test_bayesil_xml_dir
    elif bayesil_xml_dir !="":
        maple_command+=" -b_xml %s " %  bayesil_xml_dir



    if case_dss_path !="":
        maple_command+=" -dss %s " %  case_dss_path
    elif test_dss_path !="":
        maple_command+=" -dss %s " %  test_dss_path
    elif dss_path !="":
        maple_command+=" -dssl %s " %  dss_path

    if case_subtraction !="":
        maple_command+=" -s %s " %  case_subtraction
    elif test_subtraction !="":
        maple_command+=" -s %s " %  test_subtraction
    elif subtraction !="":
        maple_command+=" -s %s " %  subtraction

    #if case_cut_off_list !="":
    #    maple_command+=" -cl %s " %  case_cut_off_list
    #elif test_cut_off_list !="":
    #    maple_command+=" -cl %s " %  test_cut_off_list
    #elif cut_off_list !="":
    #    maple_command+=" -cl %s " %  cut_off_list

    if case_tail_corr !="":
        maple_command+=" -t %s " %  case_tail_corr
    elif test_tail_corr !="":
        maple_command+=" -t %s " %  test_tail_corr
    elif tail_corr !="":
        maple_command+=" -t %s " %  tail_corr


    if case_high_conc_metabolites !="":
        maple_command+=" -hi %s " %  case_high_conc_metabolites
    elif test_high_conc_metabolites !="":
        maple_command+=" -hi %s " %  test_high_conc_metabolites
    elif high_conc_metabolites !="":
        maple_command+=" -hi %s " %  high_conc_metabolites


    if case_subtraction_check_list_file !="":
        maple_command+=" -hc %s " %  case_subtraction_check_list_file
    elif test_subtraction_check_list_file !="":
        maple_command+=" -hc %s " %  test_subtraction_check_list_file
    elif subtraction_check_list_file !="":
        maple_command+=" -hc %s " %  subtraction_check_list_file



    if case_conc_cutoff !="":
        maple_command+=" -cutoff %s " %  case_conc_cutoff
    elif test_conc_cutoff !="":
        maple_command+=" -cutoff %s " %  test_conc_cutoff
    elif conc_cutoff !="":
        maple_command+=" -cutoff %s " %  conc_cutoff



    if maple_location !="":
        maple_command+=" -m %s " %  maple_location


    if profile==1:
        print "maple_command = ", [maple_command]
        os.system(maple_command)
        #print "Exiting..."
        #sys.exit() 
    

    os.chdir(cur_dir)
    debug=0
    if debug==1:
        print "Exiting after run_maple()"
        sys.exit()
    return






def parse_maple_output_all(\
        case,\
        project_dir,\
              ):
    """
    Function parse_maple_output_all()
    """
    verbose=1
    if verbose>=1 or g_verbose>=1: 
        print "Running function parse_maple_output_all()"  
        print " project_dir = ", project_dir
        print " case = ", case
    #result_file="%s/%s/Results/out_result_all_conc.csv" % (project_dir,case)
    result_file="%s/%s/Results/out_xy_before_profiling.txt_result_all_conc.csv" % (project_dir,case)

    file_lines=read_file(result_file)
    concentration_dict={}
    for i in  file_lines:
        print "i = ", i
        i=i.replace("\r\n","")
        i=i.replace("\n","")
        i_split=i.split(",")
        if len(i_split)<3:
            print "Error!!! The length of i_split < 3", [i_split]
        else:
            concentration= i_split[2]
            hmdb_id=i_split[0]
            metabolite_name=i_split[1]
            if is_number(concentration):
                concentration_dict[hmdb_id]={"metabolite_name":metabolite_name, "concentration":float(concentration)}
            else:
                print "Warning!!!!  concentration %s is not numeric for %s %s" % (concentration,hmdb_id,metabolite_name)  

        y="""
        if hmdb_id in i:
            if  metabolite_name not in i:
                print "Warning!!! metabolite_name %s is not in " % metabolite_name, i
            i_split=i.split(",")
            if len(i_split)<3:
                print "Error!!! The length of i_split < 3", [i_split]
            concentration= i_split[2] 
            if is_number(concentration)==0:
                print "concentration is not numerical and is ", [concentration]
        """

    debug=0
    if debug==1:
        print "  concentration_dict = ",   concentration_dict
        #print " concentration_float = ",  float(concentration)
        print "Exiting after parse_maple_output_all()"
        sys.exit()
    return concentration_dict















def parse_maple_output(\
        case,\
        project_dir,\
        hmdb_id,\
        metabolite_name,\
              ):
    """
    Function parse_maple_output()

[mark@localhost March_15_2018]$ ls -lrt /home/mark/Dropbox/Maple/test123/1_HMDB00008_2-Hydroxybutyrate/Results/
total 4060
-rw-rw-r-- 1 mark mark    1225 Mar 16 02:14 out_detection_all.csv
-rw-rw-r-- 1 mark mark    1188 Mar 16 02:14 out_filtered_detection_all.csv
-rw-rw-r-- 1 mark mark     118 Mar 16 02:14 out_result_all_conc.csv
-rw-rw-r-- 1 mark mark 3100767 Mar 16 02:14 out_result.json

HMDB00008,2-Hydroxybutyrate,995.372
HMDB00562,Creatinine,23.168
HMDB00000,DSS,46370.035
HMDB01875,Methanol,97.921


    """
    verbose=1
    if verbose>=1 or g_verbose>=1: 
        print "Running function parse_maple_output()"  
        print " project_dir = ", project_dir
        print " case = ", case

    #result_file="%s/%s/Results/out_result_all_conc.csv" % (project_dir,case)
    result_file="%s/%s/Results/out_xy_before_profiling.txt_result_all_conc.csv" % (project_dir,case)
    #print "result_file = ", result_file
    file_lines=read_file(result_file)
    concentration="NONE"
    for i in  file_lines:
        print "i = ", i
        if hmdb_id in i:
            if  metabolite_name not in i:
                print "Warning!!! metabolite_name %s is not in " % metabolite_name, i
            i_split=i.split(",")
            if len(i_split)<3:
                print "Error!!! The length of i_split < 3", [i_split]
            concentration= i_split[2] 
            if is_number(concentration)==0:
                print "concentration is not numerical and is ", [concentration]


    debug=1
    if debug==1:
        print " concentration = ",  concentration
        print " concentration_float = ",  float(concentration)
        print "Exiting after parse_maple_output()"
        sys.exit()
    return concentration





def write_report(\
         test_dict,\
         project_dir,\
         report_text,\
         project_name,\
            ):
    """
    Function write report
    """
    verbose=1
    if verbose>=1 or g_verbose>=1: 
        print "Running function write_report()" 
 
    result_file="%s/%s_results.csv" % (project_dir,project_name)
    write_file(result_file,report_text)
    if verbose>=1 or g_verbose>=1: 
        print "result_file = ", result_file 



    debug=0
    if debug==1:
        print "result_file= ", result_file
        print "Exiting after write_report()"
        sys.exit()
    return



def process_spectrum():
    """
    Process of a spectrum
python /home/mark/software/BayesilWorking/bayesil-app/Bayesil/bayesil.py /home/mark/Dropbox/Fixing_Bayesil_TMIC_version/tmic_bayesil_web_profile_tmp --setting /home/mark/Dropbox/Fixing_Bayesil_TMIC_version/tmic_bayesil_setting --update ReadIOSpectrum.path=/home/mark/Dropbox/Fixing_Bayesil_TMIC_version/tests_at_home/test_case_1/spectrum.fid Quantify.path=/home/mark/Dropbox/Fixing_Bayesil_TMIC_version/tests_at_home/test_case_1 Quantify.dss_conc=1000.0 AnnealedParticleMF.nr_samples=10000 SaveStatus.path=/home/mark/Dropbox/Fixing_Bayesil_TMIC_version/tests_at_home/test_case_1 SimplePlot.path=/home/mark/Dropbox/Fixing_Bayesil_TMIC_version/tests_at_home/test_case_1/plot.png Save.path=/home/mark/Dropbox/Fixing_Bayesil_TMIC_version/tests_at_home/test_case_1 SaveResultAsJson.json_file=/home/mark/Dropbox/Fixing_Bayesil_TMIC_version/tests_at_home/test_case_1/result.json LibReading.lib_folder=/home/mark/software/BayesilWorking/Libraries/500 Quantify.lb_file=/home/mark/software/BayesilWorking/Libraries/filters/lower_bound_serum_exceptions.csv SaveResultAsJson.confidence_scores=/home/mark/software/BayesilWorking/Libraries/filters/confidence_scores.txt SaveSpectrumXYPairs.path=/home/mark/Dropbox/Fixing_Bayesil_TMIC_version/tests_at_home/test_case_1 LibReading.filter_file=/home/mark/software/BayesilWorking/Libraries/filters/serum.txt  LibReading.weight_list=weightSerum.csv
    """
    return
    
#######################################################
# End of functions
#######################################################


command_text="python "
a=0

if len(sys.argv)>1:
    for item in sys.argv:

        ##############################
        # General settings
        ##############################
        if item in ["-help","-h"]:
            print usage        
            sys.exit(-1)


        elif item=="-v": 
            g_verbose=sys.argv[a+1] 


        ##############################

        ##############################
        # Project/file management 
        ##############################
        elif item=="-loc": 
            location=os.path.abspath(sys.argv[a+1]) 

        elif item=="-p": 
            project=sys.argv[a+1] 

        elif item=="-b": 
            biofluid=sys.argv[a+1] 

        elif item=="-l": 
            library_file=os.path.abspath(sys.argv[a + 1])

        elif item=="-w": 
            wt_list_file=os.path.abspath(sys.argv[a + 1])

        elif item=="-m":
            maple_location=os.path.abspath(sys.argv[a + 1])

        elif item=="-c": 
           constraint_file=os.path.abspath(sys.argv[a + 1])

        elif item in ["-m_xml"]:
            maple_xml_dir  = os.path.abspath(sys.argv[a + 1])

        elif item in ["-b_xml"]:
            bayesil_xml_dir  = os.path.abspath(sys.argv[a + 1])

        elif item=="-prof": 
            profile=int(sys.argv[a+1])

        elif item=="-proc": 
            process=int(sys.argv[a+1])

        elif item=="-profile_always": 
            profile_always=int(sys.argv[a+1])

        elif item in ["-s", "-sub"]:
            subtraction_arg = sys.argv[a + 1]
            if subtraction_arg in ["yes","Yes"]:
                subtraction = "Yes"
                print "Please provide high_conc_metabolites, subtraction_check_list_file "


        elif item in ["-t", "-tail"]:
            tail_corr_arg = sys.argv[a + 1]
            if tail_corr_arg in ["yes","Yes","y"]:
                tail_corr = "Yes"
                print "Please provide conc_cutoff list. Other wise default [1000, 500, 100] will be used. "

        elif item in ["-cl", "-c_list"]:
            cut_off_list = sys.argv[a + 1]

        elif item in ["-hi"]:
            high_conc_metabolites = os.path.abspath(sys.argv[a + 1])

        elif item in ["-hc"]:
            subtraction_check_list_file = os.path.abspath(sys.argv[a + 1])

        elif item in ["-cutoff"]:
             conc_cutoff = os.path.abspath(sys.argv[a + 1])

        elif item in ["-dss"]:
             dss_path = os.path.abspath(sys.argv[a + 1])




        a+=1


        command_text+=" %s " % item


#######################################################
#  Processing arguments
#######################################################                            
this_script_full_path=os.path.abspath(sys.argv[0])
this_script_name=os.path.basename(sys.argv[0])
script_location=os.path.dirname(this_script_full_path)
if g_verbose>0:
    print "this_script_full_path=", this_script_full_path
    print "this_script_name=", this_script_name
    print "script_location=", script_location
cur_dir=os.getcwd()
if location=="":
    location=cur_dir
project_dir="%s/%s" % (location,project)
#make_dir_no_remove(location)
#######################################################                            

main(\
    project_dir,\
    test_dict,\
    maple_location,\
    maple_script,\
    biofluid,\
    library_file,\
    wt_list_file,\
    constraint_file,\
    maple_xml_dir,\
    bayesil_xml_dir,\
    profile,\
    process,\
    profile_always,\
    make_case_library_files,\
    project,\
    subtraction,\
    tail_corr,\
    cut_off_list,\
    high_conc_metabolites,\
    subtraction_check_list_file,\
    conc_cutoff,\
    dss_path,\
    )

