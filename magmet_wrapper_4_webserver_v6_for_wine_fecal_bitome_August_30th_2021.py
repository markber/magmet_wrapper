#######################################################
# Import modules
#######################################################
import sys, os, random, copy
from os import linesep
from os.path import exists
from string import split, upper, lower
from random import shuffle
import time
import datetime
from standard_functions import *
#######################################################



#######################################################
# Default values
#######################################################
verbose=1
g_verbose=1
python_program="python"
python3_program="python"

project="" # flag "-p"
location="" # flag -"loc"
#directory_with_xml_files=""
#directory_with_xml_files_default=""
env="magmet_webserver"
#env="arnau_computer"
#env="trent_computer"

run_bayesil=1
magmet_options=""
processing_mode=""
species_type=""
spectra_location_dir=""
working_dir=""
#d_magmet_processing_program="magmet_parallel_process_profile_restart.py"
#d_magmet_processing_program="magmet_single_processing.py"
d_magmet_processing_program="magmet_parallel_processing.py"
magmet_processing_program=""
do_magmet_processing=""
d_do_magmet_processing=1
do_magmet_profiling=""
d_do_magmet_profiling=1
d_copy_result_files=1
copy_result_files=""
d_update_status=1
update_status=""

Example_of_Bayesil_command="""
python /apps/bayesil/project/releases/20180528215710/lib/bayesil/app/Bayesil/bayesil.py /apps/bayesil/project/releases/20180528215710/lib/bayesil/jobs/bayesil_web_profile.job --setting /apps/bayesil/project/releases/20180528215710/lib/bayesil/app/setting --update ReadIOSpectrum.path=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152/spectrum.fid Quantify.path=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152 Quantify.dss_conc=1000.0 AnnealedParticleMF.nr_samples=10000 SaveStatus.path=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152 SimplePlot.path=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152/plot.png Save.path=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152 SaveResultAsJson.json_file=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152/result.json LibReading.lib_folder=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Libraries/500 Quantify.lb_file=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Libraries/filters/lower_bound_serum_exceptions.csv SaveResultAsJson.confidence_scores=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Libraries/filters/confidence_scores.txt SaveSpectrumXYPairs.path=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152 LibReading.filter_file=/apps/bayesil/project/releases/20180528215710/lib/bayesil/metabolites/serum.txt LibReading.weight_list=weightSerum.csv
"""

Example_of_MAGMET_processing_command="""
~/venv/bin/python  -m . -i
data/rhonda_bell/data/all_fid/ -p data/rhonda_bell/data/ -dss
metabolite_list/dss.txt
"""

Exampes_of_magmet_command="""
 python magmet_wrapper_4_webserver_v3.py  -sl /home/mark/Dropbox/Work/Magmet_project/Magmet/test_cases/good_cases -loc /home/mark/Dropbox/Work/Magmet_project/Magmet/test_cases -p good_case -do_mproc 0 -do_mprof 0
"""

Example_of_MAGMET_processing_command="""
python  magmet_parallel_processing.py  -m . -i data/NIST/ -p data/NIST/ -dss metabolite_list/dss.txt -su settings_update_700.txt -f 700  -su settings_update_700.txt
"""

Example_of_MAGMET_profiling_command="""
python magmet_serial_profiling_Mark_dev.py  -i /apps/bayesil/project/releases/20190516143758/BayesilWorking/Results/e27cc40d0793131e8d6d92ae9a93fab2ad86d9df/13621/  -p MagmetProfiling  -loc /apps/bayesil/project/releases/20190516143758/BayesilWorking/Results/e27cc40d0793131e8d6d92ae9a93fab2ad86d9df/13621  -f 700 -b serum -sp Cow -t y -su settings_update_700.txt -m /apps/bayesil/magmet/testing/magmet-backend/
"""

#######################################################
# Bayesil arguments
#######################################################

bayesil_wrapper=""
bayesil_program=""
bayesil_web_profile=""
bayesil_setting=""
bayesil_update=0
bayesil_spectrum_dir=""
bayesil_quantify_path=""
bayesil_dss_conc=""
bayesil_AnnealedParticleMF_nr_samples=""
bayesil_SaveStatus_path=""
bayesil_SimplePlot_path=""
bayesil_Save_path=""
bayesil_SaveResultAsJson_json_file=""
bayesil_LibReading_lib_folder=""
bayesil_Quantify_lb_file=""
bayesil_SaveResultAsJson_confidence_scores_file=""
bayesil_LibReading_filter_file=""
bayesil_LibReading_weight_list=""

bayesil_argument_list=[]
setting_flag="--setting" # Done
bayesil_argument_list+=[setting_flag]
update_flag="--update"
bayesil_argument_list+=[update_flag]
ReadIOSpectrum_path_argument="ReadIOSpectrum.path=" #Done
bayesil_argument_list+=[ReadIOSpectrum_path_argument]
Quantify_path_argument="Quantify.path="#Done
bayesil_argument_list+=[Quantify_path_argument]
Quantify_dss_conc_argument="Quantify.dss_conc="
bayesil_argument_list+=[Quantify_dss_conc_argument]
AnnealedParticleMF_nr_samples_argument="AnnealedParticleMF.nr_samples="
bayesil_argument_list+=[AnnealedParticleMF_nr_samples_argument]
SaveStatus_path_argument="SaveStatus.path="
bayesil_argument_list+=[SaveStatus_path_argument]
SimplePlot_path_argument="SimplePlot.path="
bayesil_argument_list+=[SimplePlot_path_argument]
Save_path_argument="Save.path="
bayesil_argument_list+=[Save_path_argument]
SaveResultAsJson_json_file_argument="SaveResultAsJson.json_file="
bayesil_argument_list+=[SaveResultAsJson_json_file_argument]
LibReading_lib_folder_argument="LibReading.lib_folder="
bayesil_argument_list+=[LibReading_lib_folder_argument]
Quantify_lb_file_argument="Quantify.lb_file="
bayesil_argument_list+=[Quantify_lb_file_argument]
SaveResultAsJson_confidence_scores_argument="SaveResultAsJson.confidence_scores=" 
bayesil_argument_list+=[SaveResultAsJson_confidence_scores_argument]
SaveSpectrumXYPairs_path_argument="SaveSpectrumXYPairs.path=" 
bayesil_argument_list+=[SaveSpectrumXYPairs_path_argument]
SaveSpectrumXYPairs_xy_file_argument="SaveSpectrumXYPairs.xy_file=" 
bayesil_argument_list+=[SaveSpectrumXYPairs_xy_file_argument]
LibReading_filter_file_argument="LibReading.filter_file="
bayesil_argument_list+=[LibReading_filter_file_argument]
LibReading_weight_list_argument="LibReading.weight_list=" 
bayesil_argument_list+=[LibReading_weight_list_argument]
bayesil_Exponential_Line_Broadening_flag="ExponentialLineBroadening.LB=" 
bayesil_argument_list+=[bayesil_Exponential_Line_Broadening_flag]


#######################################################
# Magmet defailts
#######################################################
magmet_dir="" # flag "-m"
data_dir ="" # flags "-i", "-sp"
subtraction_arg = "" # flags "-s", "-sub"
tail_corr_arg ="" # flags "-t", "-tail"


##################################
# Biofluid
##################################
bio_fluid_type=""# flags "-b", "-bio"
##################################

##################################
# Weight list
##################################
wt_list_file=""# flags "-w", "-wt"
##################################

##################################
# Constraint file
##################################
constraint_file=""# flags "-c", "-con"
##################################

##################################
# Library file
##################################
library_file=""# flags "-l", "-lib"
##################################

##################################
# magmet XML dir
##################################
magmet_xml_dir=""# flag -"-m_xml"dir/"
##################################

##################################
# Bayesil XML dir
##################################
bayesil_xml_dir="" # flag -"-b_xml"
##################################

##################################
# Dss list
##################################
dss_path=""# flags "-dss"
##################################

##################################
# check for tail_correction
##################################
tail_corr=""
##################################

##################################
# concentration cutoff
##################################
conc_cutoff="" # flags "-cutoff"
##################################

##################################
# check for cutoff list
##################################
cut_off_list=""# flags "-cl", "-c_list"
##################################

##################################
# check for subtraction
##################################
subtraction=""
##################################

##################################
# high conc metabolites
##################################
high_conc_metabolites=""  # flags "-hi"
##################################

##################################
# subtraction check_ ist file
##################################
subtraction_check_list_file="" # flags "-hc"
##################################

##################################
# Spectra path
##################################
spectra_path="" # flags "-i", "-sp"
##################################

##################################
# out_put name
##################################
out_put_name="" # flags -"-o", "-out"
##################################

##################################
# field_strength
##################################
field_strength = "" # flags -"-f"
##############################

##################################
# baseline correction
##################################
baseline_correction = ""
##################################


##################################
# iteration
##################################
iteration = ""
##################################



##################################
# iteration_i
##################################
iteration_i = ""
##################################


##################################
# cut_off_list
##################################
cut_off_list = ""
##################################

##################################
# noise_mask_list
##################################
noise_mask_list = ""
##################################



#######################################################
#Dictionaries
#######################################################
env_dict={}
env_dict["magmet_webserver"]={}
env_dict["arnau_computer"]={}


# MAGMET options
#env_dict["magmet_webserver"]["python_for_magmet"]="/apps/bayesil/.pyenv/versions/anaconda-1.9.2/envs/py2715/bin/python"

# For serum magmet
#env_dict["magmet_webserver"]["python_for_magmet"]="/apps/bayesil/Python_2_7_16/bin/python"

# For fecal magmet
env_dict["magmet_webserver"]["python_for_magmet"]="/apps/bayesil/miniconda3/envs/py37/bin/python"
#env_dict["magmet_webserver"]["python_for_magmet_for_wine_and_beer"]="/apps/bayesil/Python_2_7_16/bin/python"
env_dict["magmet_webserver"]["python_for_magmet_for_wine_and_beer"]="/apps/bayesil/miniconda3/envs/py37/bin/python"
env_dict["magmet_webserver"]["python_for_magmet_for_bitome"]="/apps/bayesil/miniconda3/envs/py37/bin/python"




#env_dict["magmet_webserver"]["magmet_program"]="maple_bruker_process_profile.py"
#env_dict["magmet_webserver"]["magmet_program"]="maple_main_30_may.py"
#env_dict["magmet_webserver"]["magmet_program"]="maple_main_June_25th_2018.py"
#env_dict["magmet_webserver"]["magmet_program"]="magmet_serial_profiling_Mark_dev.py"

#For serum magmet
#env_dict["magmet_webserver"]["magmet_program"]="magmet_single_profiling.py"

#For fecal magmet
env_dict["magmet_webserver"]["magmet_program"]="magmet_parallel_profiling.py"



#env_dict["magmet_webserver"]["magmet_location"]="/apps/bayesil/magmet/magmet-sexsmith"
#env_dict["magmet_webserver"]["magmet_location"]="/apps/bayesil/magmet/Magmet_August_15th_2018"
#env_dict["magmet_webserver"]["magmet_location"]="/apps/bayesil/magmet/MagMet_August_20th_2018"

# For serum magmet
#env_dict["magmet_webserver"]["magmet_location"]="/apps/bayesil/magmet/magmet-backend"
#For fecal magmet
env_dict["magmet_webserver"]["magmet_location"]="/apps/bayesil/magmet/magmet_for_fecal_waters"
#env_dict["magmet_webserver"]["magmet_location_for_wine_and_beer"]="/apps/bayesil/magmet/magmet_for_wine_and_beer/magmet_bw_July_8th_2021"
#env_dict["magmet_webserver"]["magmet_location_for_wine_and_beer"]="/apps/bayesil/magmet/magmet_for_wine_and_beer/magmet_fecal_for_wine"
env_dict["magmet_webserver"]["magmet_location_for_wine_and_beer"]="/apps/bayesil/magmet/magmet_backend_wine_August_30th_2021"
#env_dict["magmet_webserver"]["magmet_location_for_bitome"]="/apps/bayesil/magmet/magmet-for-bitome/magmet-backend_60"
env_dict["magmet_webserver"]["magmet_location_for_bitome"]="/apps/bayesil/magmet/magmet_for_bitome_August_30th_2021"


# For serum magmet
#env_dict["magmet_webserver"]["biofluid"]="serum"

#For fecal magmet
env_dict["magmet_webserver"]["biofluid"]="fecal"


# Old , do not use
#env_dict["magmet_webserver"]["wt_list_file"]=env_dict["magmet_webserver"]["magmet_location"]+"/wt_list/serum_weight.xlsx"
#env_dict["magmet_webserver"]["wt_list_file"]=env_dict["magmet_webserver"]["magmet_location"]+"/wt_list/serum_weight_Mark.xlsx"
#env_dict["magmet_webserver"]["constraint_file"]=env_dict["magmet_webserver"]["magmet_location"]+"/constrain_list/constrain_serum.xlsx"
#env_dict["magmet_webserver"]["library_file"]=env_dict["magmet_webserver"]["magmet_location"]+"/metabolite_list/serum.txt"
#env_dict["magmet_webserver"]["magmet_xml_dir"]=env_dict["magmet_webserver"]["magmet_location"]+"/maple_xml_dir/"
#env_dict["magmet_webserver"]["bayesil_xml_dir"]=env_dict["magmet_webserver"]["magmet_location"]+"/bayesil_xml_dir/"
#env_dict["magmet_webserver"]["dss_path"]=env_dict["magmet_webserver"]["magmet_location"]+"/metabolite_list/dss.txt"
#env_dict["magmet_webserver"]["high_conc_metabolites"]=env_dict["magmet_webserver"]["magmet_location"]+"/high_conc/beetle_high_conc.txt"
#env_dict["magmet_webserver"]["subtraction_check_list_file"]=env_dict["magmet_webserver"]["magmet_location"]+"/high_conc/subtraction_check_list.txt"



# Weights - not used
#env_dict["magmet_webserver"]["wt_list_file"]="wt_list/serum_weight_sheep.xlsx"
#env_dict["magmet_webserver"]["wt_list_file"]="wt_list/serum_weight_pig_Mark_1.xlsx"
#env_dict["magmet_webserver"]["wt_list_file"]="wt_list/Beetle_weight.xlsx"
#env_dict["magmet_webserver"]["wt_list_file"]="wt_list/old_serum_weight_sheep copy.xlsx"
#env_dict["magmet_webserver"]["wt_list_file"]="wt_list/rumen_weight.xlsx"
env_dict["magmet_webserver"]["wt_list_file"]="wt_list/serum_weight.xlsx"
#env_dict["magmet_webserver"]["wt_list_file"]="wt_list/serum_weight_pig.xlsx"



# Constraints- not used
env_dict["magmet_webserver"]["constraint_file"]="constrain_list/constrain_serum.xlsx"
#env_dict["magmet_webserver"]["constraint_file"]="constrain_list/format_constrain_serum.xlsx"
#env_dict["magmet_webserver"]["constraint_file"]="constrain_list/constrain_serum_pig.xlsx"
#env_dict["magmet_webserver"]["constraint_file"]="constrain_list/constrain_serum_sheep.xlsx"


# Metabolite lists - not used
#env_dict["magmet_webserver"]["library_file"]="metabolite_list/acetone_lib.txt"
#env_dict["magmet_webserver"]["library_file"]="metabolite_list/serum_new_EDTA_woHisMeHis.txt"
#env_dict["magmet_webserver"]["library_file"]="metabolite_list/beetle_lib.txt"
#env_dict["magmet_webserver"]["library_file"]="metabolite_list/rumen.txt"
#env_dict["magmet_webserver"]["library_file"]="metabolite_list/serum_new_EDTA.txt"
env_dict["magmet_webserver"]["library_file"]={"field":{}}
env_dict["magmet_webserver"]["library_file"]["field"]["default"]="metabolite_list/serum.txt"
env_dict["magmet_webserver"]["library_file"]["field"]["60"]="metabolite_list/serum_new.txt"
env_dict["magmet_webserver"]["library_file"]["field"]["700"]="metabolite_list/serum_new.txt"
env_dict["magmet_webserver"]["library_file"]["field"]["500"]="metabolite_list/serum.txt"
env_dict["magmet_webserver"]["library_file"]["field"]["600"]="metabolite_list/serum.txt"
env_dict["magmet_webserver"]["library_file"]["field"]["800"]="metabolite_list/serum.txt"

#env_dict["magmet_webserver"]["library_file"]="metabolite_list/serum.txt"
#env_dict["magmet_webserver"]["library_file"]="metabolite_list/metabolite_list_73.txt"


env_dict["magmet_webserver"]["magmet_xml_dir"]="maple_xml_dir/"
env_dict["magmet_webserver"]["bayesil_xml_dir"]="bayesil_xml_dir/"
env_dict["magmet_webserver"]["dss_path"]="metabolite_list/dss.txt"
env_dict["magmet_webserver"]["high_conc_metabolites"]="high_conc/beetle_high_conc.txt"
env_dict["magmet_webserver"]["subtraction_check_list_file"]="high_conc/subtraction_check_list.txt"





env_dict["magmet_webserver"]["tail_corr"]="y"
env_dict["magmet_webserver"]["cut_off_list"]="1000,500,200"
env_dict["magmet_webserver"]["subtraction"]="No"
env_dict["magmet_webserver"]["conc_cutoff"]=3000
env_dict["magmet_webserver"]["spectra_path"]=""
env_dict["magmet_webserver"]["out_put_name"]="out_"
env_dict["magmet_webserver"]["field_strength"]=700.23
env_dict["magmet_webserver"]["baseline_correction"]=None
env_dict["magmet_webserver"]["iteration"]=20
env_dict["magmet_webserver"]["iteration_i"]=10
env_dict["magmet_webserver"]["cut_off_list"]="1000,500,200,100,50"
env_dict["magmet_webserver"]["noise_mask_list"]="10,1,0.2"



biofluid_default_field=700
biofluid_dict={}
biofluid_dict["serum"]={}
biofluid_dict["serum"]["fields"]={}
biofluid_dict["serum"]["fields"][60]={}
biofluid_dict["serum"]["fields"][500]={}
biofluid_dict["serum"]["fields"][600]={}
biofluid_dict["serum"]["fields"][700]={}
biofluid_dict["serum"]["fields"][700]["cut_off_list"]=[1000,500,200,100,50]
biofluid_dict["serum"]["fields"][700]["noise_mask_list"]=[10,1,0.2]
#biofluid_dict["serum"]["fields"][700]["noise_mask_list"]=[10,1,0.0001]
biofluid_dict["serum"]["fields"][800]={}




Example_of_Bayesil_command="""
# DONE python /apps/bayesil/project/releases/20180528215710/lib/bayesil/app/Bayesil/bayesil.py 

# DONE /apps/bayesil/project/releases/20180528215710/lib/bayesil/jobs/bayesil_web_profile.job 

# DONE --setting /apps/bayesil/project/releases/20180528215710/lib/bayesil/app/setting 

# DONE --update ReadIOSpectrum.path=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152/spectrum.fid 

# DONE Quantify.path=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152 

# DONE Quantify.dss_conc=1000.0 

# DONE AnnealedParticleMF.nr_samples=10000 

# DONE SaveStatus.path=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152 

# DONE SimplePlot.path=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152/plot.png 

# DONE Save.path=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152 

# DONE SaveResultAsJson.json_file=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152/result.json 


# DONE LibReading.lib_folder=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Libraries/500 

# DONE Quantify.lb_file=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Libraries/filters/lower_bound_serum_exceptions.csv

# DONE SaveResultAsJson.confidence_scores=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Libraries/filters/confidence_scores.txt 

# DONE SaveSpectrumXYPairs.path=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152 

DONE LibReading.filter_file=/apps/bayesil/project/releases/20180528215710/lib/bayesil/metabolites/serum.txt 

DONE LibReading.weight_list=weightSerum.csv

"""



# Bayesil options
env_dict["magmet_webserver"]["link_to_bayesil_wrapper"]="/apps/bayesil/project/shared/lib/bayesil/app/Bayesil/bayesil.py"
env_dict["magmet_webserver"]["link_to_bayesil_program"]="/apps/bayesil/project/shared/lib/bayesil/app/Bayesil/bayesil_old.py"
#env_dict["magmet_webserver"]["link_to_job_file"]="/apps/bayesil/project/shared/lib/bayesil/app/Jobs/bayesil_web_profile.job"
#env_dict["magmet_webserver"]["link_to_job_file"]="/apps/bayesil/project/current/lib/bayesil/jobs/bayesil_web_profile.job"
env_dict["magmet_webserver"]["link_to_job_file"]="/apps/bayesil/magmet/magmet_wrapper/magmet.job"
env_dict["magmet_webserver"]["link_to_setting"]="/apps/bayesil/project/current/lib/bayesil/app/setting"
env_dict["magmet_webserver"]["link_to_spectrum_dir"]="/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152/spectrum.fid"
env_dict["magmet_webserver"]["Quantify_path"]="/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152"
env_dict["magmet_webserver"]["Quantify_dss_conc"]=1000.0 
env_dict["magmet_webserver"]["AnnealedParticleMF_nr_samples"]=10000
env_dict["magmet_webserver"]["SaveStatus_path"]="/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152"
env_dict["magmet_webserver"]["SimplePlot_path"]="/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152/plot.png"
env_dict["magmet_webserver"]["Save_path"]="/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152"
env_dict["magmet_webserver"]["SaveResultAsJson_json_file"]="/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152/result.json"
env_dict["magmet_webserver"]["LibReading_lib_folder"]="/apps/bayesil/project/shared/BayesilWorking/Libraries/500"
env_dict["magmet_webserver"]["Quantify_lb_file"]="/apps/bayesil/project/shared/BayesilWorking/Libraries/filters/lower_bound_serum_exceptions.csv"
env_dict["magmet_webserver"]["SaveResultAsJson_confidence_scores_file"]="/apps/bayesil/project/shared/BayesilWorking/Libraries/filters/confidence_scores.txt"
env_dict["magmet_webserver"]["SaveSpectrumXYPairs_path"]="/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152"
#env_dict["magmet_webserver"]["SaveSpectrumXYPairs_xy_file"]="xy_spectrum.txt"
env_dict["magmet_webserver"]["LibReading_filter_file"]="/apps/bayesil/project/shared/lib/bayesil/metabolites/serum.txt"
env_dict["magmet_webserver"]["LibReading_weight_list"]="weightSerum.csv"
env_dict["magmet_webserver"]["bayesil_Exponential_Line_Broadening"]=0.5

# Magmet processing
env_dict["magmet_webserver"]["processing_mode"]=1
env_dict["magmet_webserver"]["do_magmet_processing"]=1
env_dict["magmet_webserver"]["spectra_location_dir"]=""
#env_dict["magmet_webserver"]["magmet_processing_program"]="magmet_parallel_process_profile_restart.py"

# For serum
#env_dict["magmet_webserver"]["magmet_processing_program"]="magmet_single_processing.py"
# For fecal
env_dict["magmet_webserver"]["magmet_processing_program"]="magmet_parallel_processing.py"




env_dict["magmet_webserver"]["do_magmet_profiling"]=1
env_dict["magmet_webserver"]["copy_result_files"]=1
env_dict["magmet_webserver"]["update_status"]=1


env_dict["arnau_computer"]=copy.deepcopy(env_dict["magmet_webserver"])
env_dict["arnau_computer"]["magmet_location"]="/home/mark/research/magmet_testing/Magmet_August_1st_2018"
env_dict["arnau_computer"]["processing_mode"]=1
env_dict["arnau_computer"]["do_magmet_processing"]=1
env_dict["arnau_computer"]["spectra_location_dir"]=""
#env_dict["arnau_computer"]["magmet_processing_program"]="magmet_parallel_process_profile_restart.py"
env_dict["arnau_computer"]["magmet_processing_program"]="magmet_single_processing.py"
#env_dict["arnau_computer"]["magmet_processing_program"]="magmet_parallel_processing.py"

env_dict["arnau_computer"]["copy_result_files"]=0
env_dict["arnau_computer"]["update_status"]=0



env_dict["trent_computer"]=copy.deepcopy(env_dict["arnau_computer"])
env_dict["trent_computer"]["magmet_location"]="/home/mark/Dropbox/Work/Magmet_project/Magmet/Magmet_August_13th_2018"
env_dict["trent_computer"]["python_for_magmet"]="/home/mark/anaconda2/bin/python"




#######################################################
# Functions
#######################################################
def main(\
		project_name,\
		bayesil_command,\
		run_bayesil,\
		working_dir,\
		python_4_magmet,\
		magmet_program,\
		biofluid,\
		library,\
		weight_list,\
		magmet_location,\
		dss_file_location,\
		magmet_options,\
		tail_corr,\
		field_strength,\
		cut_off_list,\
		noise_mask_list,\
		iteration,\
		iteration_i,\
		processing_mode,\
		do_magmet_processing,\
		spectra_location_dir,\
		magmet_processing_program,\
		do_magmet_profiling,\
		copy_result_files,\
		update_status,\
		constraint_file,\
		species_type,\
		):
	"""
	Main function
	"""
	verbose = 0
	if verbose >= 1:
		print "Running function Main()"
	project_name="MagmetProfiling"

	if processing_mode==0:
		run_bayesil_processing(\
			bayesil_command,\
			run_bayesil,\
			)
		x_y_spectrum="%s/xy_processed.txt" % working_dir
	else:
		x_y_spectrum=run_magmet_processing(\
				do_magmet_processing,\
				working_dir,\
				python_4_magmet,\
				magmet_processing_program,\
				project_name,\
				magmet_location,\
				spectra_location_dir,\
				field_strength,\
				)                 







	if file_exists(x_y_spectrum)==0:
		print "ERROR!!! The spectrum %s was not generated" % x_y_spectrum
		print "Exiting..."
		sys.exit()

	run_magmet_profiling(\
		working_dir,\
		python_4_magmet,\
		magmet_program,\
		x_y_spectrum,\
		project_name,\
		biofluid,\
		library,\
		weight_list,\
		magmet_location,\
		dss_file_location,\
		magmet_options,\
		tail_corr,\
		field_strength,\
		cut_off_list,\
		noise_mask_list,\
		iteration,\
		iteration_i,\
		do_magmet_profiling,\
		constraint_file,\
		species_type,\
		)

	if copy_result_files==1:

		copy_results(\
		working_dir,\
		project_name,\
		processing_mode,\
		)

	if update_status==1:
		update_status_file(\
		working_dir,\
		)

	debug=1
	if debug==1:
		print "Exiting after function main()"
		sys.exit()
	return


def run_magmet_profiling(\
		working_dir,\
		python_4_magmet,\
		magmet_program,\
		x_y_spectrum,\
		project_name,\
		biofluid,\
		library,\
		weight_list,\
		magmet_location,\
		dss_file_location,\
		magmet_options,\
		tail_corr,\
		field_strength,\
		cut_off_list,\
		noise_mask_list,\
		iteration,\
		iteration_i,\
		do_magmet_profiling,\
		constraint_file,\
		species,\
		):
	"""
	Run Magmet profiling
/apps/bayesil/.pyenv/versions/anaconda-1.9.2/envs/py2715/bin/python  
python maple_main_19_march.py  
-i /apps/bayesil/magmet/magmet-app/tests_at_home/nist_serum_from_tmic_bayesil/xy_before_profiling.txt  
-loc /apps/bayesil/magmet/magmet-app/tests_at_home  


-p nist_serum_on_server_June_18_2018  
-b serum  
-l /apps/bayesil/magmet/magmet-app/metabolite_list/serum_new.txt  
-w wt_list/serum_weight.xlsx   
-m /apps/bayesil/magmet/magmet-app
-dss  /apps/bayesil/magmet/magmet-app/metabolite_list/dss.txt



python magmet_parallel_profiling_Mark_dev.py  -i /apps/bayesil/project/releases/20190516143758/BayesilWorking/Results/e27cc40d0793131e8d6d92ae9a93fab2ad86d9df/13621/processed_spectrum.fid.txt  -p MagmetProfiling  -loc /apps/bayesil/project/releases/20190516143758/BayesilWorking/Results/e27cc40d0793131e8d6d92ae9a93fab2ad86d9df/13621  -b serum  -l metabolite_list/serum_new.txt  -w wt_list/serum_weight.xlsx  -c constrain_list/constrain_serum.xlsx  -m /apps/bayesil/magmet/magmet-sexsmith  -dss metabolite_list/dss.txt  -t y  -f 700


 ~/venv/bin/python magmet_parallel_profiling.py -m . -i data/TMIC0011B/Results/ -p data/TMIC0011B/Results/ -f 700 -b serum -sp Cow -t y -su settings_update_700.txt


	"""
	verbose = 1
	if verbose >= 1:
		print "Running function run_magmet()"

	magmet_command=""


	if 	python_4_magmet=="":
		print "ERROR!!! python_4_magmet is empty. Exiting..."
		sys.exit()
	else:
		magmet_command+="%s " % python_4_magmet


	if 	magmet_program=="":
		print "ERROR!!! magmet_program is empty. Exiting..."
		sys.exit()
	else:
		magmet_command+="%s " % magmet_program


	if x_y_spectrum=="":
		print "ERROR!!! x_y_spectrum is empty. Exiting..."
		sys.exit()
	else:							
		#magmet_command+=" -i %s " % x_y_spectrum
		magmet_command+=" -i %s " % working_dir


	if project_name=="":
		print "ERROR!!! project_name is empty. Exiting..."
		sys.exit()
	else:							
		magmet_command+=" -p %s " % project_name


	if working_dir=="":
		print "ERROR!!! working_dir is empty. Exiting..."
		sys.exit()
	else:							
		magmet_command+=" -loc %s " % working_dir


	if biofluid=="":
		print "ERROR!!! biofluid is empty. Exiting..."
		sys.exit()
	else:							
		magmet_command+=" -b %s " % biofluid



	if species=="":
		print "ERROR!!! species is empty. Exiting..."
		sys.exit()
	else:							
		magmet_command+=" -sp %s " % species



	#if library=="":
	#	print "ERROR!!! library is empty. Exiting..."
	#	sys.exit()
	#else:							
	#	magmet_command+=" -l %s " % library


	#if weight_list=="":
	#	print "ERROR!!! weight_list is empty. Exiting..."
	#	sys.exit()
	#else:							
	#	magmet_command+=" -w %s " % weight_list


	#if constraint_file=="":
	#	print "ERROR!!! constraint_file is empty. Exiting..."
	#	sys.exit()
	#else:							
	#	magmet_command+=" -c %s " % constraint_file


	if magmet_location=="":
		print "ERROR!!! magmet_location is empty. Exiting..."
		sys.exit()
	else:							
		magmet_command+=" -m %s " % magmet_location


	#if dss_file_location=="":
	#	print "ERROR!!! dss_file_location is empty. Exiting..."
	#	sys.exit()
	#else:							
	#	magmet_command+=" -dss %s " % dss_file_location

	#disabled_on_January_9th_2019="""
	if tail_corr=="":
		print "Warning!!! tail_corr is not set"
	else:							
		magmet_command+=" -t %s " % tail_corr
	#"""


	if field_strength=="":
		print "Warning!!! field_strength is not set. Using 700MHz frequency"
	else:							
		magmet_command+=" -f %s -su settings_update_%s.txt" % (field_strength,field_strength)

	disabled_on_January_9th_2019="""
	if iteration=="":
		print "Warning!!! iteration is not set. Using the default value"
	else:
		magmet_command+=" -iter %s " % iteration

	if iteration_i=="":
		print "Warning!!! iteration_i is not set. Using the default value"
	else:
		magmet_command+=" -iter_i %s " % iteration		


	if cut_off_list==[]:
		print "Warning!!! iteration_i is not set. Using the default value"	
	else:
		cut_off_list_string=""
		for b in cut_off_list:
			if cut_off_list_string not in ["","."]:
				cut_off_list_string+=","
			cut_off_list_string+="%s" % b
		magmet_command+=" -cl %s " % cut_off_list_string


	if noise_mask_list==[]:
		print "Warning!!! iteration_i is not set. Using the default value"	
	else:
		noise_mask_list_string=""
		for b in noise_mask_list:
			if noise_mask_list_string!="":
				noise_mask_list_string+=","
			noise_mask_list_string+="%s" % b
		magmet_command+=" -nl %s " % noise_mask_list_string
	"""

	cur_dir=os.getcwd()
	os.chdir(magmet_location)

	print "MAGMET command: ", magmet_command
	if do_magmet_profiling==1:
		os.system(magmet_command)

	os.chdir(cur_dir)
	debug=0
	if debug==1:
		print "Exiting after function run_magmet()"
		sys.exit()

	return



def run_magmet_processing(\
		do_magmet_processing,\
		working_dir,\
		python_4_magmet,\
		magmet_processing_program,\
		project_name,\
		magmet_location,\
		spectra_location_dir,\
		field_strength,\
		):
	"""
	Run magmet processing

/home/mark/anaconda2/bin/python  magmet_parallel_process_profile_restart.py -m . -i /home/mark/Dropbox/Work/Magmet_project/Magmet/test_cases/good_cases/ -p  RK01 -dss metabolite_list/dss.txt

	"""
	verbose = 1
	if verbose >= 1:
		print "Running function run_magmet_processing()"
		print "do_magmet_processing = ", do_magmet_processing
		print "working_dir = ", working_dir
		print "python_4_magmet = ", python_4_magmet
		print "magmet_processing_program = ", magmet_processing_program
		print "project_name = ", project_name
		print "magmet_location = ", magmet_location
		print "spectra_location_dir = ", spectra_location_dir
		print "field_strength = ", field_strength


	cur_dir=os.getcwd()

	output_file=""
	field_option=""

	#else:
	#	if project_name=="":
	#		print "Warning!!! project_name is not defined "
	#		print "Exiting..."
	#		sys.exit()
	#	else:
	#		output_file = "%s/%s" % (working_dir,project_name)
	#		make_dir_no_remove(working_dir)

			

	if verbose >= 1:
		print "Current directory is ", cur_dir
		print "Changing directory to ", magmet_location

	if field_strength!="":
		field_option=" -f %s -su settings_update_%s.txt " % (field_strength, field_strength)	


	if magmet_location=="":
		print "Warning!!! magmet_location is not defined "
		print "Exiting..."
		sys.exit()
	else:
		if spectra_location_dir=="":
			print "Warning!!! spectra_location_dir  is not defined "
			print "Exiting..."
			sys.exit()
		else:


			debug=0
			if debug==1:
				print "working_dir = ", working_dir
				sys.exit()


			dir_list=os.listdir(spectra_location_dir)
			if len(dir_list)==0:
				print "Error!!! spectra_location_dir %s is empty " % spectra_location_dir
				print "Exiting..."
				sys.exit()

			#elif len(dir_list)>1:
			#	print "Error!!! spectra_location_dir %s has more than one file " % spectra_location_dir
			#	print "Exiting..."
			#	sys.exit()
			else:
				dir_name=dir_list[0]

				if working_dir=="":
					print "Warning!!! working_dir is not defined "
					print "Exiting..."
					sys.exit()
				else:
					make_dir_no_remove(working_dir)
					#output_file = "%s/%s" % (working_dir,dir_name)
					output_option= "%s/processed_" % (working_dir)
					output_file= "%s/processed_%s.txt" % (working_dir,dir_name)				
					
				
					os.chdir(magmet_location)
					magmet_processing_command="%s %s -m . -i %s/ -o %s -dss metabolite_list/dss.txt %s" % (python_4_magmet,magmet_processing_program, spectra_location_dir, output_option, field_option)

					if verbose >= 1:
						print "magmet_processing_command is ", magmet_processing_command

					if do_magmet_processing==1:			
						result_of_magmet_processing=os.system(magmet_processing_command)
						print "result_of_magmet_processing = ", result_of_magmet_processing
						print "output_file = ", output_file
						if file_exists(output_file)==0:
							print "Error!!!! output_file %s  has not been generated " % output_file

			os.chdir(cur_dir)

	debug=0
	if debug==1:
		print "Exiting after function run_magmet_processing()"
	 	print "output_file = ", output_file
		sys.exit()
	return   output_file               




def run_bayesil_processing(\
		bayesil_command,\
		run_bayesil,\
		):
	"""
	Run Bayesil processing
	"""
	verbose = 1
	if verbose >= 1:
		print "Running function run_bayesil_processing()"

	if run_bayesil==1:
		print "bayesil_command= ", bayesil_command
		os.system(bayesil_command)

	debug=0
	if debug==1:
		print "Exiting after function run_bayesil_processing()"
		sys.exit()
	return

def copy_results(\
		working_dir,\
		project_name,\
		processing_mode,\
		):
	"""
	Run Bayesil processing
	-rw-rw-r-- 1 bayesil bayesil   30772 Jun 14 02:20 out_13621_processed_spectrum.fid.txt_cluster_data_all_with_multiplication_factor.csv
	-rw-rw-r-- 1 bayesil bayesil    1845 Jun 14 02:37 out_13621_processed_spectrum.fid.txt_result_all_conc.csv
	-rw-rw-r-- 1 bayesil bayesil 4585064 Jun 14 02:37 out_13621_processed_spectrum.fid.txt_result.json
	-rw-rw-r-- 1 bayesil bayesil    1888 Jun 14 02:37 out_13621_combined_all.csv
	"""
	verbose = 1
	if verbose >= 1:
		print "Running function copy_results()"

	case=os.path.basename(working_dir)
	if processing_mode==0:
		link_to_json_file="%s/%sResults/out_xy_processed.txt_result.json" % (working_dir,project_name)
		

	else:
		#link_to_json_file="%s/%sResults/out_processed_spectrum.fid.txt_result.json" % (working_dir,project_name)
		link_to_json_file="%s/%sResults/out_%s_processed_spectrum.fid.txt_result.json" % (working_dir,project_name,case)

	#link_to_json_file="%s/%sResults/out*result.json" % (working_dir,project_name)
	final_json_file="%s/result.json" % working_dir


	debug=0
	if debug==1:
		print "link_to_json_file = ", link_to_json_file
		print "final_json_file = ", final_json_file
		print "Exiting..."
		sys.exit()



	if processing_mode==0:	
		link_to_csv_file="%s/%sResults/out_xy_processed.txt_result_all_conc.csv" % (working_dir,project_name)
	else:
		#link_to_csv_file="%s/%sResults/out_processed_spectrum.fid.txt_result_all_conc.csv" % (working_dir,project_name)
		link_to_csv_file="%s/%sResults/out_%s_processed_spectrum.fid.txt_result_all_conc.csv" % (working_dir,project_name,case)


	final_csv_file="%s/bayesil.csv" % working_dir
        final_csv_file2="%s/magmet.csv" % working_dir	

	if file_exists(link_to_json_file)==0:
		print "Warning!!! JSON file %s does not exist" % link_to_json_file
		print "Exiting..."
		sys.exit()
	else:
		copy_command="cp %s %s" % (link_to_json_file, final_json_file)
		print "copy_command=", copy_command		
		json_copy_result=os.system(copy_command)
		print "json_copy_result = ",	json_copy_result


	if file_exists(link_to_csv_file)==0:
		print "Warning!!! csv file %s does not exist" % link_to_csv_file
		print "Exiting..."
		sys.exit()
	else:
		copy_command="cp %s %s" % (link_to_csv_file, final_csv_file)	
		print "copy_command=", copy_command		
		csv_copy_result=os.system(copy_command)	
		print "csv_copy_result = ",	csv_copy_result


		copy_command2="cp %s %s" % (link_to_csv_file, final_csv_file2)
		print "copy_command2=", copy_command2
		csv_copy_result2=os.system(copy_command2)
		print "csv_copy_result2 = ",     csv_copy_result2



	debug=0
	if debug==1:
		print "Exiting after function copy_results()"
		sys.exit()
	return


def update_status_file(\
		    working_dir,\
		):
	"""
	Run update status file
	"""
	verbose = 1
	if verbose >= 1:
		print "Running function update_status_file()"
	time.sleep(30)
	status_file="%s/status" %  working_dir
	if file_exists(status_file)==1:
		remove_file_command="rm %s" %  status_file
		os.system(remove_file_command)
	text="Complete"	
	write_file(status_file, text)				

	debug=0
	if debug==1:
		print "Exiting after function update_status_file()"
		sys.exit()
	return



def convert_csv_string_2_list_of_floats(l_string):
    """
    Convert CSV string 2 list of float numbers
    """
    verbose=1
    if verbose>=1:
        print "Running function convert_csv_string_2_list_of_floats()"    
    l_list=[]
    string_split=l_string.split(",")
    for b in string_split:
        f=b.split(" ")
        if len(f)>0:
            d=f[0]
            if is_number(d)==1:
                l_list+=[float(d)]
    debug=1
    if debug==1:
        print "Exiting after convert_csv_string_2_list_of_floats()"
        sys.exit()
    return l_list


#######################################################
# Pasing arguments
#######################################################

command_text="python "
bayesil_command=""
i=0

if len(sys.argv)>1:
	for item in sys.argv:
		##############################
		# Project/file management
		##############################
		#if item=="-l":
		#	location=os.path.abspath(sys.argv[i+1])
		#if item=="-p":
		#	project=sys.argv[i+1]
		if item=="-env":
			env=sys.argv[i+1]
			if env not in env_dict:
				print "Warning!!! env %s is not in env_dict" % env			
		#elif item=="-exp_dir":
		#	experiment_dir=sys.argv[i+1]
		#elif item=="-xml_dir":
		#	directory_with_xml_files=os.path.abspath(sys.argv[i+1])

		if item in ["-h", "-help"]:
			print help
			sys.exit()

		if item in ["-pm"]:
			processing_mode=int(sys.argv[i + 1])

		if item in ["-do_mproc"]:
			do_magmet_processing=int(sys.argv[i + 1])

		if item in ["-do_mprof"]:
			do_magmet_profiling=int(sys.argv[i + 1])

		if item in ["-sl"]:
			spectra_location_dir=os.path.abspath(sys.argv[i+1])


		##############################
		# Magmet arguments
		##############################		

		if item in ["-i", "-sp"]:
			# print "input file location", os.path.abspath(sys.argv[i+1])
			# if sys.argv[i+1][0]="-"
			#data_dir = os.path.abspath(sys.argv[i + 1])
			spectra_path = os.path.abspath(sys.argv[i + 1])


		if item in ["-o", "-out"]:
			out_put_name = os.path.abspath(sys.argv[i + 1])

		if item in ["-b", "-bio"]: #Done
			bio_fluid_type = sys.argv[i + 1]
			#print "bio_fluid type ", bio_fluid_type

		if item in ["-l", "-lib"]:
			library_file = os.path.abspath(sys.argv[i + 1])

		if item in ["-w", "-wt"]: # Done
			t_list_file = os.path.abspath(sys.argv[i + 1])

		if item in ["-c", "-con"]: #Done
			constraint_file = os.path.abspath(sys.argv[i + 1])

		if item in ["-s", "-sub"]:
			subtraction_arg = sys.argv[i + 1]
			if subtraction_arg in ["yes","Yes"]:
				subtraction = "Yes"
				#print "Please provide high_conc_metabolites, subtraction_check_list_file "

		if item in ["-t", "-tail"]:
			tail_corr_arg = sys.argv[i + 1]
			if tail_corr_arg in ["yes","Yes","y"]:
				tail_corr = "y"
        		#print "Please provide conc_cutoff list. Other wise default [1000, 500, 100] will be used. "

		# Mark
		if sys.argv[i] in ["-cl", "-c_list"]:
			cut_off_list_string = sys.argv[i + 1]
			cut_off_list=convert_csv_string_2_list_of_floats(cut_off_list_string)

		# Mark
		if sys.argv[i] in ["-nl", "-n_list"]:
			noise_mask_list_string = sys.argv[i + 1]
			noise_mask_list=convert_csv_string_2_list_of_floats(noise_mask_list_string)

		# Mark
		if sys.argv[i] in ["-iter"]:
			iteration_string = sys.argv[i + 1]
			if is_number(iteration_string)==1:
				iteration=int(iteration_string)
        
		# Mark
		if sys.argv[i] in ["-iter_i"]:
			iteration_i_string = sys.argv[i + 1]
			if is_number(iteration_i_string)==1:
				iteration_i=int(iteration_i_string)

		if item in ["-hi"]:
			high_conc_metabolites = os.path.abspath(sys.argv[i + 1])
		if item in ["-hc"]:
			subtraction_check_list_file = os.path.abspath(sys.argv[i + 1])

		if item in ["-bc","-base"]:
			baseline_correction = os.path.abspath(sys.argv[i + 1])
			#print "baseline_correction ",baseline_correction	

		if item in ["-cutoff"]:
			conc_cutoff= sys.argv[i + 1]

		if item in ["-dss"]:
			dss_path = os.path.abspath(sys.argv[i + 1])

		if item in ["-f"]:  # Done
			field_strength = sys.argv[i + 1]
			#print "field_strength ", field_strength

		if item in ["-bc","-base"]:
			baseline_correction = os.path.abspath(sys.argv[i + 1])
			#print "baseline_correction ",baseline_correction

		# Mark:
		if item in ["-mp"]:
			magmet_processing_program  = sys.argv[i + 1]
		


		#Mark:
		if item in ["-m"]: # Done
			magmet_dir  = os.path.abspath(sys.argv[i + 1])

		#Mark:
		if item in ["-m_xml"]:
			magmet_xml_dir  = os.path.abspath(sys.argv[i + 1])

		#Mark:
		if item in ["-b_xml"]:
			bayesil_xml_dir  = os.path.abspath(sys.argv[i + 1])

		# Mark:
		if item in ["-loc"]:
			location  = os.path.abspath(sys.argv[i + 1])

		# Mark:
		if item in ["-p"]:
			project  = sys.argv[i + 1]
		




		##############################
		# Baeysil arguments
		##############################		

		# Mark:

		if item in ["-rb"]:
			run_bayesil  = int(sys.argv[i + 1])		

		# Link to Mark's Bayesil wrapper
		if item in ["-bw"]:
			bayesil_wrapper  = sys.argv[i + 1]	


		# Link to Bayesil Python script, add to command separately
		if item in ["-bp"]:
			bayesil_program  = sys.argv[i + 1]		


		# Link to Bayesil web profile, add to command separately
		if i==2:		
			bayesil_web_profile=os.path.abspath(item)
		# Overwriting Bayesil web profile file if "-wp" flag is used
		if item in ["-wp"]:
			bayesil_web_profile=sys.argv[i + 1]	

		# Path Bayesil settings	
		if item in ["--setting"]:
			bayesil_setting=os.path.abspath(sys.argv[i + 1])
			bayesil_command+=" --setting %s " % bayesil_setting

		# Bayesil update flag	
		if item in ["--update"]:
			bayesil_update=1
			bayesil_command+=" --update "

		if "ReadIOSpectrum.path" in item and "=" in item:			
			bayesil_spectrum_dir=os.path.abspath(item.split("=")[1])
			#bayesil_command+=" %s " % item

		if "Quantify.path" in item and "=" in item:			
			bayesil_quantify_path=os.path.abspath(item.split("=")[1])
			#bayesil_command+=" %s " % item

		if "Quantify.dss_conc" in item and "=" in item:
			bayesil_dss_conc_string=item.split("=")[1]
			if is_number(bayesil_dss_conc_string)==1:			
				bayesil_dss_conc=float(bayesil_dss_conc_string)
				#bayesil_command+=" %s " % item
			else:
				print "Warning!!! Bayesil Quantify.dss_conc is misformatted and is ", [item]	

		if "SaveStatus.path" in item and "=" in item:
			bayesil_SaveStatus_path=os.path.abspath(item.split("=")[1])
			#bayesil_command+=" %s " % item	

		if "SimplePlot.path" in item and "=" in item:
			bayesil_SimplePlot_path=os.path.abspath(item.split("=")[1])
			#bayesil_command+=" %s " % item

		if "SaveResultAsJson.json_file" in item and "=" in item:
			bayesil_SaveResultAsJson_json_file=os.path.abspath(item.split("=")[1])
			#bayesil_command+=" %s " % item			

		if "LibReading.lib_folder" in item and "=" in item:
			bayesil_LibReading_lib_folder=os.path.abspath(item.split("=")[1])
			#bayesil_command+=" %s " % item		

		if "Quantify.lb_file" in item and "=" in item:
			bayesil_Quantify_lb_file=os.path.abspath(item.split("=")[1])
			#bayesil_command+=" %s " % item			

		if "SaveResultAsJson.confidence_scores" in item and "=" in item:
			bayesil_SaveResultAsJson_confidence_scores_file=os.path.abspath(item.split("=")[1])
			#bayesil_command+=" %s " % item		

		if "SaveSpectrumXYPairs.path" in item and "=" in item:
			bayesil_SaveSpectrumXYPairs_path=os.path.abspath(item.split("=")[1])
			#bayesil_command+=" %s " % item			

		if "LibReading.filter_file" in item and "=" in item:
			bayesil_LibReading_filter_file=os.path.abspath(item.split("=")[1])
			#bayesil_command+=" %s " % item			

		if "LibReading.weight_list" in item and "=" in item:
			bayesil_LibReading_weight_list=item.split("=")[1]
			#bayesil_command+=" %s " % item	

		if "ExponentialLineBroadening.LB" in item and "=" in item:
			bayesil_Exponential_Line_Broadening_string=item.split("=")[1]
			if is_number(bayesil_Exponential_Line_Broadening_string)==1:			
				bayesil_Exponential_Line_Broadening=float(bayesil_Exponential_Line_Broadening_string)
				#bayesil_command+=" %s " % item
			else:
				print "Warning!!! Bayesil ExponentialLineBroadening.LB is misformatted and is ", [item]	

		if "=" in item:
			item_found=0
			bayesil_command+=" %s " % item
			for argument in bayesil_argument_list:
				if argument in item:
					item_found=1
			if item_found==0:
				print "Warning!!!! Argument %s is not in bayesil_argument_list"	%  item					

		i+=1
	command_text+=" %s " % item


###########################################
# End of pasing arguments
#######################################################



#######################################################
#  Processing general arguments
#######################################################							
this_script_full_path=os.path.abspath(sys.argv[0])
this_script_name=os.path.basename(sys.argv[0])
script_location=os.path.dirname(this_script_full_path)
if g_verbose>0:
	print "this_script_full_path=", this_script_full_path
	print "this_script_name=", this_script_name
	print "script_location=", script_location
cur_dir=os.getcwd()
if location=="":
	location=cur_dir
project_dir="%s/%s" % (location,project)


debug=0
if debug==1:
	print "project_dir = ", project_dir
	sys.exit()

#######################################################
#  End of processing general arguments
#######################################################	

##################################
# Processing Magmet arguments
##################################

##################################
# Biofluid
##################################
d_bio_fluid_type="serum"

bio_fluid_type_list=["serum","general","fecal", "wine", "beer","bitome","media","Bitome","Media"]

# if "serum" in bayesil_LibReading_filter_file:
# 	bio_fluid_type="serum"

if bio_fluid_type=="":

	for bio_fluid in bio_fluid_type_list:

		if bio_fluid in bayesil_LibReading_filter_file:

			bio_fluid_type=bio_fluid
			if bio_fluid_type in ["bitome","Media","Bitome"]:
				bio_fluid_type="media"
			
if bio_fluid_type=="":
	if "biofluid" in env_dict[env]:
		bio_fluid_type=env_dict[env]["biofluid"]	

if bio_fluid_type=="":
	bio_fluid_type=d_bio_fluid_type


##################################



##################################
# MagMet location
##################################
if magmet_dir!="":
	magmet_location=magmet_dir
else:
	if env not in env_dict:
		print "Warning!!! env %s is not in env_dict" % env
	else:
		if "magmet_location" not in env_dict[env]:
			print "Warning!!! magmet_location is not in env_dict[%s]" % env
		else:
			if bio_fluid_type in ["wine","beer"]:
				magmet_location=env_dict[env]["magmet_location_for_wine_and_beer"]
			elif bio_fluid_type=="media":
				magmet_location=env_dict[env]["magmet_location_for_bitome"]
			else:	
				magmet_location=env_dict[env]["magmet_location"]
			print "magmet_location = ", magmet_location
if magmet_location!="":
	d_magmet_dir=magmet_location
else:
	d_magmet_dir=os.getcwd()	




debug=0
if debug==1:
	print "magmet_dir =", magmet_dir
	print "Exiting after magmet_location set-up..."
	sys.exit()





y="""
drwxrwxr-x 3 bayesil bayesil 4096 Jun 12 03:21 fecal
drwxrwxr-x 5 bayesil bayesil 4096 Jun 12 03:21 general
drwxrwxr-x 8 bayesil bayesil 4096 Jun 12 03:21 serum
[bayesil@do-magmet-dev-1-june-20-2018-s-2vcpu-4gb-nyc2-01 magmet-backend]$ ls -lrt profiling_input_files/700/serum/
total 24
drwxrwxr-x 3 bayesil bayesil 4096 Jun 12 03:21 Horse
drwxrwxr-x 4 bayesil bayesil 4096 Jun 12 03:21 Pig
drwxrwxr-x 2 bayesil bayesil 4096 Jun 12 03:21 QCs
drwxrwxr-x 3 bayesil bayesil 4096 Jun 12 03:21 Sheep
drwxrwxr-x 3 bayesil bayesil 4096 Jun 13 16:50 Cow
drwxrwxr-x 3 bayesil bayesil 4096 Jun 13 16:50 Human
"""

##################################
# Species
##################################
d_species_type="Human"

species_type_list=["Human","Horse","Pig","QCs","Sheep","Cow", "default","Bitome","bitome","media","Media"]

if "Human" in bayesil_LibReading_filter_file:
	species_type="Human"


if species_type=="":

	for species in species_type_list:

		if species in bayesil_LibReading_filter_file:

			species_type=species

			if species_type in ["Bitome","bitome","media","Media"]:
				species_type="media"					


if species_type=="":
	if "species" in env_dict[env]:
		species_type=env_dict[env]["species"]	

if species_type=="":
	species_type=d_species_type
if bio_fluid_type in ["wine","beer"]:
	species_type="default"

	
##################################


##################################
# Weight list
##################################
#d_wt_list_file = d_magmet_dir + "/wt_list/serum_weight.xlsx"
d_wt_list_file = d_magmet_dir + "/wt_list/serum_weight_Mark.xlsx"
##################################


##################################
# Constraint file
##################################
d_constraint_file = d_magmet_dir + "/constrain_list/constrain_serum.xlsx"
##################################


##################################
# Library file
##################################
d_library_file = d_magmet_dir + "/metabolite_list/serum.txt"
##################################


##################################
# magmet XML dir
##################################
d_magmet_xml_dir = d_magmet_dir + "/magmet_xml_dir/"
##################################


##################################
# Bayesil XML dir
##################################
d_bayesil_xml_dir = d_magmet_dir + "/bayesil_xml_dir/"
##################################


##################################
# Dss list
##################################
d_dss_path = d_magmet_dir + "/metabolite_list/dss.txt"
##################################

##################################
# Set tail_correction
##################################
d_tail_corr = "No" # Default tail correction

if tail_corr == "":
	if env not in env_dict:
		print "Warning!!! Environment %s is not in  env_dict" % env
	else:
		if "tail_corr" not in env_dict[env]:
			print "Warning!!! tail_corr is not in env_dict[%s]" % env
		else:
			tail_corr=env_dict[env]["tail_corr"]
			#magmet_options+=" -t %s " % tail_corr	
#else:
#	magmet_options+=" -t %s " % tail_corr	

if tail_corr == "":
	tail_corr=d_tail_corr
	#magmet_options+=" -t %s " % tail_corr	


##################################


##################################
# Setting iteration #Mark
##################################
d_iteration = 20
if iteration=="":
	if env in env_dict and "iteration" in env_dict[env]:
		iteration=env_dict[env]["iteration"]
if 	iteration=="":
	iteration=d_iteration
##################################

##################################
# Setting iteration_i #Mark
##################################
d_iteration_i = 10
if iteration_i=="":
	if env in env_dict and "iteration_i" in env_dict[env]:
		iteration=env_dict[env]["iteration_i"]
if 	iteration_i=="":
	iteration_i=d_iteration_i
##################################


##################################
# check for subtraction
##################################
d_subtraction = "No"
##################################


##################################
# high conc metabolites
##################################
d_high_conc_metabolites = d_magmet_dir + "/high_conc/beetle_high_conc.txt"
##################################

##################################
# subtraction_check_list_file
##################################
d_subtraction_check_list_file = d_magmet_dir + "/high_conc/subtraction_check_list.txt"
##################################

##################################
# concentration cutoff
##################################
d_conc_cutoff = 3000
##################################

##################################
# Spectra path
##################################
d_spectra_path = "/Volumes/GoogleDrive/Team Drives/magmet_python/bayesil_2/data/S1-1.fid.txt"
##################################

##################################
# out_put name
##################################
d_out_put_name = "out_"
##################################

#################################
# Field strength
##################################
d_field_strength = 700
if "500" in bayesil_LibReading_lib_folder:
	field_strength=500
if "600" in bayesil_LibReading_lib_folder:
	field_strength=600
if "700" in bayesil_LibReading_lib_folder:
	field_strength=700
if "800" in bayesil_LibReading_lib_folder:
	field_strength=800
if "60" in bayesil_LibReading_lib_folder:
	field_strength=60	
if field_strength=="":
	field_strength=d_field_strength
field_strength_string="%s" % field_strength
##################################


##############################

if bio_fluid_type in ["wine","beer"]:
	if "python_for_magmet_for_wine_and_beer" in env_dict[env]:
		python_for_magmet=env_dict[env]["python_for_magmet_for_wine_and_beer"]
	else:
		print "ERROR!!! python_for_magmet_for_wine_and_beer is not in env_dict[%s]" % env
		print "Exiting..."
		sys.exit()
else:
	if "python_for_magmet" in env_dict[env]:
		python_for_magmet=env_dict[env]["python_for_magmet"]
	else:
		print "ERROR!!! python_for_magmet is not in env_dict[%s]" % env
		print "Exiting..."
		sys.exit()			

if "magmet_program" in env_dict[env]:
	magmet_program=env_dict[env]["magmet_program"]
else:
	print "ERROR!!! magmet_program is not in env_dict[%s]" % env
	print "Exiting..."
	sys.exit()



##############################################
# Setting up metabolite list
##############################################
if library_file=="":
	if "library_file" in env_dict[env]:

		if "field" not in env_dict[env]["library_file"]:
			print """Warning!!!  field is not in env_dict[env]["library_file"]"""
			print "library=d_library_file", d_library_file
			library=d_library_file
		else:
			if field_strength_string not in env_dict[env]["library_file"]["field"]:
				print """Warning!!!  field %s is not in env_dict[env]["library_file"]["field"]""" % field_strength_string
				print "library=d_library_file", d_library_file	
				library=d_library_file
			else:			
				library=env_dict[env]["library_file"]["field"][field_strength_string]
	else:
		library=d_library_file
		print "Warning!!!! library_file is not in env_dict[%s]" % env
		print "library=d_library_file", d_library_file
##############################################

if "constraint_file" in env_dict[env]:
	constraint_file=env_dict[env]["constraint_file"]
else:
	constraint_file=d_wt_list_file
	print "Warning!!!! constraint_file is not in env_dict[%s]" % env
	print "constraint_file=d_constraint_file", constraint_file


if "wt_list_file" in env_dict[env]:
	weight_list=env_dict[env]["wt_list_file"]
else:
	weight_list=d_wt_list_file
	print "Warning!!!! wt_list_file is not in env_dict[%s]" % env
	print "weight_list=d_wt_list_file", d_wt_list_file






if "dss_path" in env_dict[env]:
	dss_file_location=env_dict[env]["dss_path"]
else:
	dss_file_location=d_dss_path
	print "Warning!!!! dss_path is not in env_dict[%s]" % env
	print "dss_file_location=d_dss_path", d_dss_path









#######################################################
#  End of processing Magmet arguments
#######################################################	


#######################################################
#  Processing Bayesil arguments
#######################################################	


#######################################################
#  Setting a job file for Bayesil
#######################################################	


bayesil_web_profile_2_use=""

if env not in env_dict:
	print "Warning!!! env %s is not in env_dict" % env
else:
	if "link_to_job_file" not in env_dict[env]:
		print "Warning!!! link_to_job_file is not in env_dict[%s]" % env
	else:
		bayesil_web_profile_2_use=env_dict[env]["link_to_job_file"]

if bayesil_web_profile_2_use=="" and bayesil_web_profile!="":
	bayesil_web_profile_2_use=bayesil_web_profile

if bayesil_web_profile_2_use=="":
	print "ERROR!!! bayesil_web_profile  was not found"
	print "The program will exit now"
	sys.exit()
else:
	bayesil_command=" %s " % bayesil_web_profile_2_use + bayesil_command





#######################################################
#  Setting Bayesil Python program
#######################################################	


bayesil_program_2_use=""

if env not in env_dict:
	print "Warning!!! env %s is not in env_dict" % env
else:
	if "link_to_bayesil_program" not in env_dict[env]:
		print "Warning!!! link_to_bayesil_program is not in env_dict[%s]" % env
	else:
		bayesil_program_2_use=env_dict[env]["link_to_bayesil_program"]



#######################################################
#  End of processing arguments
#######################################################	

##################################
# Setting cutoff list
##################################
#d_cut_off_list = [500, 200, 100] # For standards
d_cut_off_list = [1000, 500, 200, 100, 50] # For serum # Mark
if cut_off_list=="":
	if bio_fluid_type in biofluid_dict:
		if field_strength in biofluid_dict[bio_fluid_type]["fields"]:
			if "cut_off_list" in biofluid_dict[bio_fluid_type]["fields"][field_strength]:
				cut_off_list= biofluid_dict[bio_fluid_type]["fields"][field_strength]["cut_off_list"]
	if cut_off_list=="":			
		if env in env_dict and "cut_off_list" in env_dict[env]:
			cut_off_list=env_dict[env]["cut_off_list"]
if 	cut_off_list=="":
	cut_off_list=d_cut_off_list
##################################


##################################
# Setting noise mask list #Mark
##################################
#d_noise_mask_list = [10,1,0.0001]
d_noise_mask_list = [10,1,0.2]

if noise_mask_list=="":
	if bio_fluid_type in biofluid_dict:
		if field_strength in biofluid_dict[bio_fluid_type]["fields"]:
			if "noise_mask_list" in biofluid_dict[bio_fluid_type]["fields"][field_strength]:
				noise_mask_list= biofluid_dict[bio_fluid_type]["fields"][field_strength]["noise_mask_list"]
	if noise_mask_list=="":			
		if env in env_dict and "noise_mask_list" in env_dict[env]:
			noise_mask_list=env_dict[env]["noise_mask_list"]
if 	noise_mask_list=="":
	noise_mask_list=d_noise_mask_list



d_processing_mode=1
if processing_mode=="":
    if "processing_mode" in env_dict[env]:
	    processing_mode=env_dict[env]["processing_mode"]
    else:
	    processing_mode=d_processing_mode
	    print "Warning!!!! processing_mode is not in env_dict[%s]" % env
	    print "processing_mode=d_processing_mode", d_processing_mode




if do_magmet_processing=="":
    if "do_magmet_processing" in env_dict[env]:
	    do_magmet_processing=env_dict[env]["do_magmet_processing"]
    else:
	    do_magmet_processing=d_do_magmet_processing
	    print "Warning!!!! do_magmet_processing is not in env_dict[%s]" % env
	    print "do_magmet_processing=d_do_magmet_processing", d_do_magmet_processing


if do_magmet_profiling=="":
    if "do_magmet_profiling" in env_dict[env]:
	    do_magmet_profiling=env_dict[env]["do_magmet_profiling"]
    else:
	    do_magmet_profiling=d_do_magmet_profiling
	    print "Warning!!!! do_magmet_profiling is not in env_dict[%s]" % env
	    print "do_magmet_profiling=d_do_magmet_profiling", d_do_magmet_profiling





if magmet_processing_program=="":
    if "magmet_processing_program" in env_dict[env]:
	    magmet_processing_program=env_dict[env]["magmet_processing_program"]
    else:
	    d_magmet_processing_program=d_magmet_processing_program
	    print "Warning!!!! magmet_processing_program is not in env_dict[%s]" % env
	    print "magmet_processing_program=d_magmet_processing_program", d_magmet_processing_program


if bayesil_program_2_use=="" and bayesil_program!="":
	bayesil_program_2_use=bayesil_program
if bayesil_program_2_use=="":

	if processing_mode==0:
		print "ERROR!!! bayesil_program  was not found"
		print "The program will exit now"
		sys.exit()
else:
	bayesil_command="python %s " % bayesil_program_2_use + bayesil_command

if bayesil_quantify_path=="":

	if processing_mode==0:
		print "ERROR!!! bayesil_quantify_path  was not found"
		print "The program will exit now"
		sys.exit()	
else:	
	working_dir= spectra_location_dir=bayesil_quantify_path


d_spectra_location_dir=1
if spectra_location_dir=="":
    if "spectra_location_dir" in env_dict[env]:
	    spectra_location_dir=env_dict[env]["spectra_location_dir"]
    else:
	    d_spectra_location_dir=d_spectra_location_dir
	    print "Warning!!!! spectra_location_dir is not in env_dict[%s]" % env
	    print "spectra_location_dir=d_spectra_location_dir", d_spectra_location_dir


if copy_result_files=="":
    if "copy_result_files" in env_dict[env]:
	    copy_result_files=env_dict[env]["copy_result_files"]
    else:
	    d_copy_result_files=d_copy_result_files
	    print "Warning!!!! copy_result_files is not in env_dict[%s]" % env
	    print "copy_result_files=d_copy_result_files", d_copy_result_files


if update_status=="":
    if "update_status" in env_dict[env]:
	    update_status=env_dict[env]["update_status"]
    else:
	    d_update_status=d_update_status
	    print "Warning!!!! update_status is not in env_dict[%s]" % env
	    print "update_status=d_update_status", d_update_status






if working_dir=="":
	working_dir=project_dir
debug=0
if debug==1:
	print "working_dir = ", working_dir
	sys.exit()

##################################

#######################################################
#  Adding line broadening to bayesil command
#######################################################	

#if bayesil_Exponential_Line_Broadening_flag not in bayesil_command:
#	if "bayesil_Exponential_Line_Broadening" in env_dict[env]:
#		bayesil_Exponential_Line_Broadening_string=env_dict[env]["bayesil_Exponential_Line_Broadening"]
#		if is_number(bayesil_Exponential_Line_Broadening_string)==0:
#			print "Warning!!! bayesil_Exponential_Line_Broadening is not numerical and is ",[bayesil_Exponential_Line_Broadening_string]
#		else:
#			bayesil_Exponential_Line_Broadening_float=float(bayesil_Exponential_Line_Broadening_string)
#			Line_Broadening_argument="%s%s" % (bayesil_Exponential_Line_Broadening_flag,bayesil_Exponential_Line_Broadening_float)
#			bayesil_command=bayesil_command + " %s " % Line_Broadening_argument

#######################################################
#  Main function
#######################################################	


main(\
		project,\
		bayesil_command,\
		run_bayesil,\
		working_dir,\
		python_for_magmet,\
		magmet_program,\
		bio_fluid_type,\
		library,\
		weight_list,\
		magmet_location,\
		dss_file_location,\
		magmet_options,\
		tail_corr,\
		field_strength,\
		cut_off_list,\
		noise_mask_list,\
		iteration,\
		iteration_i,\
        processing_mode,\
        do_magmet_processing,\
		spectra_location_dir,\
		magmet_processing_program,\
		do_magmet_profiling,\
		copy_result_files,\
		update_status,\
		constraint_file,\
		species_type,\
		)

print "The Magmet wrapper has finished"
