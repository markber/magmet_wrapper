#######################################################
# Import modules
#######################################################
import sys, os, random, copy
from os import linesep
from os.path import exists
from string import split, upper, lower
from random import shuffle
import time
import datetime
from standard_functions import *
#######################################################



#######################################################
# Default values
#######################################################
verbose=1
g_verbose=1
python_program="python"
project="" # flag "-p"
location="" # flag -"loc"
#directory_with_xml_files=""
#directory_with_xml_files_default=""
env="magmet_webserver"
run_bayesil=1

Example_of_Bayesil_command="""
python /apps/bayesil/project/releases/20180528215710/lib/bayesil/app/Bayesil/bayesil.py /apps/bayesil/project/releases/20180528215710/lib/bayesil/jobs/bayesil_web_profile.job --setting /apps/bayesil/project/releases/20180528215710/lib/bayesil/app/setting --update ReadIOSpectrum.path=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152/spectrum.fid Quantify.path=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152 Quantify.dss_conc=1000.0 AnnealedParticleMF.nr_samples=10000 SaveStatus.path=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152 SimplePlot.path=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152/plot.png Save.path=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152 SaveResultAsJson.json_file=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152/result.json LibReading.lib_folder=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Libraries/500 Quantify.lb_file=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Libraries/filters/lower_bound_serum_exceptions.csv SaveResultAsJson.confidence_scores=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Libraries/filters/confidence_scores.txt SaveSpectrumXYPairs.path=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152 LibReading.filter_file=/apps/bayesil/project/releases/20180528215710/lib/bayesil/metabolites/serum.txt LibReading.weight_list=weightSerum.csv
"""



#######################################################
# Bayesil arguments
#######################################################

bayesil_wrapper=""
bayesil_program=""
bayesil_web_profile=""
bayesil_setting=""
bayesil_update=0
bayesil_spectrum_dir=""
bayesil_quantify_path=""
bayesil_dss_conc=""
bayesil_AnnealedParticleMF_nr_samples=""
bayesil_SaveStatus_path=""
bayesil_SimplePlot_path=""
bayesil_Save_path=""
bayesil_SaveResultAsJson_json_file=""
bayesil_LibReading_lib_folder=""
bayesil_Quantify_lb_file=""
bayesil_SaveResultAsJson_confidence_scores_file=""
bayesil_LibReading_filter_file=""
bayesil_LibReading_weight_list=""

bayesil_argument_list=[]
setting_flag="--setting" # Done
bayesil_argument_list+=[setting_flag]
update_flag="--update"
bayesil_argument_list+=[update_flag]
ReadIOSpectrum_path_argument="ReadIOSpectrum.path=" #Done
bayesil_argument_list+=[ReadIOSpectrum_path_argument]
Quantify_path_argument="Quantify.path="#Done
bayesil_argument_list+=[Quantify_path_argument]
Quantify_dss_conc_argument="Quantify.dss_conc="
bayesil_argument_list+=[Quantify_dss_conc_argument]
AnnealedParticleMF_nr_samples_argument="AnnealedParticleMF.nr_samples="
bayesil_argument_list+=[AnnealedParticleMF_nr_samples_argument]
SaveStatus_path_argument="SaveStatus.path="
bayesil_argument_list+=[SaveStatus_path_argument]
SimplePlot_path_argument="SimplePlot.path="
bayesil_argument_list+=[SimplePlot_path_argument]
Save_path_argument="Save.path="
bayesil_argument_list+=[Save_path_argument]
SaveResultAsJson_json_file_argument="SaveResultAsJson.json_file="
bayesil_argument_list+=[SaveResultAsJson_json_file_argument]
LibReading_lib_folder_argument="LibReading.lib_folder="
bayesil_argument_list+=[LibReading_lib_folder_argument]
Quantify_lb_file_argument="Quantify.lb_file="
bayesil_argument_list+=[Quantify_lb_file_argument]
SaveResultAsJson_confidence_scores_argument="SaveResultAsJson.confidence_scores=" 
bayesil_argument_list+=[SaveResultAsJson_confidence_scores_argument]
SaveSpectrumXYPairs_path_argument="SaveSpectrumXYPairs.path=" 
bayesil_argument_list+=[SaveSpectrumXYPairs_path_argument]
SaveSpectrumXYPairs_xy_file_argument="SaveSpectrumXYPairs.xy_file=" 
bayesil_argument_list+=[SaveSpectrumXYPairs_xy_file_argument]
LibReading_filter_file_argument="LibReading.filter_file="
bayesil_argument_list+=[LibReading_filter_file_argument]
LibReading_weight_list_argument="LibReading.weight_list=" 
bayesil_argument_list+=[LibReading_weight_list_argument]
bayesil_Exponential_Line_Broadening_flag="ExponentialLineBroadening.LB=" 
bayesil_argument_list+=[bayesil_Exponential_Line_Broadening_flag]


#######################################################
# Magmet defailts
#######################################################
magmet_dir="" # flag "-m"
data_dir ="" # flags "-i", "-sp"
subtraction_arg = "" # flags "-s", "-sub"
tail_corr_arg ="" # flags "-t", "-tail"


##################################
# Biofluid
##################################
bio_fluid_type=""# flags "-b", "-bio"
##################################

##################################
# Weight list
##################################
wt_list_file=""# flags "-w", "-wt"
##################################

##################################
# Constraint file
##################################
constraint_file=""# flags "-c", "-con"
##################################

##################################
# Library file
##################################
library_file=""# flags "-l", "-lib"
##################################

##################################
# magmet XML dir
##################################
magmet_xml_dir=""# flag -"-m_xml"dir/"
##################################

##################################
# Bayesil XML dir
##################################
bayesil_xml_dir="" # flag -"-b_xml"
##################################

##################################
# Dss list
##################################
dss_path=""# flags "-dss"
##################################

##################################
# check for tail_correction
##################################
tail_corr=""
##################################

##################################
# concentration cutoff
##################################
conc_cutoff="" # flags "-cutoff"
##################################

##################################
# check for cutoff list
##################################
cut_off_list=""# flags "-cl", "-c_list"
##################################

##################################
# check for subtraction
##################################
subtraction=""
##################################

##################################
# high conc metabolites
##################################
high_conc_metabolites=""  # flags "-hi"
##################################

##################################
# subtraction check_ ist file
##################################
subtraction_check_list_file="" # flags "-hc"
##################################

##################################
# Spectra path
##################################
spectra_path="" # flags "-i", "-sp"
##################################

##################################
# out_put name
##################################
out_put_name="" # flags -"-o", "-out"
##################################

##################################
# field_strength
##################################
field_strength = "" # flags -"-f"
##############################

##################################
# baseline correction
##################################
baseline_correction = ""
##################################


##################################
# iteration
##################################
iteration = ""
##################################



##################################
# iteration_i
##################################
iteration_i = ""
##################################


##################################
# cut_off_list
##################################
cut_off_list = ""
##################################

##################################
# noise_mask_list
##################################
noise_mask_list = ""
##################################



#######################################################
#Dictionaries
#######################################################
env_dict={}
env_dict["magmet_webserver"]={}


# MAGMET options
env_dict["magmet_webserver"]["python_for_magmet"]="/apps/bayesil/.pyenv/versions/anaconda-1.9.2/envs/py2715/bin/python"
#env_dict["magmet_webserver"]["magmet_program"]="maple_bruker_process_profile.py"
env_dict["magmet_webserver"]["magmet_program"]="maple_main_19_march.py"
env_dict["magmet_webserver"]["magmet_location"]="/apps/bayesil/magmet/MAGMET_June1st_2018"
env_dict["magmet_webserver"]["biofluid"]="serum"
env_dict["magmet_webserver"]["wt_list_file"]=env_dict["magmet_webserver"]["magmet_location"]+"/wt_list/serum_weight.xlsx"
env_dict["magmet_webserver"]["constraint_file"]=env_dict["magmet_webserver"]["magmet_location"]+"/constrain_list/constrain_serum.xlsx"
env_dict["magmet_webserver"]["library_file"]=env_dict["magmet_webserver"]["magmet_location"]+"/metabolite_list/serum.txt"
env_dict["magmet_webserver"]["magmet_xml_dir"]=env_dict["magmet_webserver"]["magmet_location"]+"/maple_xml_dir/"
env_dict["magmet_webserver"]["bayesil_xml_dir"]=env_dict["magmet_webserver"]["magmet_location"]+"/bayesil_xml_dir/"
env_dict["magmet_webserver"]["dss_path"]=env_dict["magmet_webserver"]["magmet_location"]+"/metabolite_list/dss.txt"
env_dict["magmet_webserver"]["high_conc_metabolites"]=env_dict["magmet_webserver"]["magmet_location"]+"/high_conc/beetle_high_conc.txt"
env_dict["magmet_webserver"]["subtraction_check_list_file"]=env_dict["magmet_webserver"]["magmet_location"]+"/high_conc/subtraction_check_list.txt"


env_dict["magmet_webserver"]["tail_corr"]="tail_corr"
env_dict["magmet_webserver"]["cut_off_list"]="1000,500,200"
env_dict["magmet_webserver"]["subtraction"]="No"
env_dict["magmet_webserver"]["conc_cutoff"]=3000
env_dict["magmet_webserver"]["spectra_path"]=""
env_dict["magmet_webserver"]["out_put_name"]="out_"
env_dict["magmet_webserver"]["field_strength"]=700.23
env_dict["magmet_webserver"]["baseline_correction"]=None
env_dict["magmet_webserver"]["iteration"]=20
env_dict["magmet_webserver"]["iteration_i"]=10
env_dict["magmet_webserver"]["cut_off_list"]="1000,500,200,100,50"
env_dict["magmet_webserver"]["noise_mask_list"]="10,1,0.2"



Example_of_Bayesil_command="""
# DONE python /apps/bayesil/project/releases/20180528215710/lib/bayesil/app/Bayesil/bayesil.py 

# DONE /apps/bayesil/project/releases/20180528215710/lib/bayesil/jobs/bayesil_web_profile.job 

# DONE --setting /apps/bayesil/project/releases/20180528215710/lib/bayesil/app/setting 

# DONE --update ReadIOSpectrum.path=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152/spectrum.fid 

# DONE Quantify.path=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152 

# DONE Quantify.dss_conc=1000.0 

# DONE AnnealedParticleMF.nr_samples=10000 

# DONE SaveStatus.path=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152 

# DONE SimplePlot.path=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152/plot.png 

# DONE Save.path=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152 

# DONE SaveResultAsJson.json_file=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152/result.json 


# DONE LibReading.lib_folder=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Libraries/500 

# DONE Quantify.lb_file=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Libraries/filters/lower_bound_serum_exceptions.csv

# DONE SaveResultAsJson.confidence_scores=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Libraries/filters/confidence_scores.txt 

# DONE SaveSpectrumXYPairs.path=/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152 

DONE LibReading.filter_file=/apps/bayesil/project/releases/20180528215710/lib/bayesil/metabolites/serum.txt 

DONE LibReading.weight_list=weightSerum.csv

"""



# Bayesil options
env_dict["magmet_webserver"]["link_to_bayesil_wrapper"]="/apps/bayesil/project/shared/lib/bayesil/app/Bayesil/bayesil.py"
env_dict["magmet_webserver"]["link_to_bayesil_program"]="/apps/bayesil/project/shared/lib/bayesil/app/Bayesil/bayesil_old.py"
#env_dict["magmet_webserver"]["link_to_job_file"]="/apps/bayesil/project/shared/lib/bayesil/app/Jobs/bayesil_web_profile.job"
#env_dict["magmet_webserver"]["link_to_job_file"]="/apps/bayesil/project/current/lib/bayesil/jobs/bayesil_web_profile.job"
env_dict["magmet_webserver"]["link_to_job_file"]="/apps/bayesil/magmet/magmet_wrapper/magmet.job"
env_dict["magmet_webserver"]["link_to_setting"]="/apps/bayesil/project/current/lib/bayesil/app/setting"
env_dict["magmet_webserver"]["link_to_spectrum_dir"]="/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152/spectrum.fid"
env_dict["magmet_webserver"]["Quantify_path"]="/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152"
env_dict["magmet_webserver"]["Quantify_dss_conc"]=1000.0 
env_dict["magmet_webserver"]["AnnealedParticleMF_nr_samples"]=10000
env_dict["magmet_webserver"]["SaveStatus_path"]="/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152"
env_dict["magmet_webserver"]["SimplePlot_path"]="/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152/plot.png"
env_dict["magmet_webserver"]["Save_path"]="/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152"
env_dict["magmet_webserver"]["SaveResultAsJson_json_file"]="/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152/result.json"
env_dict["magmet_webserver"]["LibReading_lib_folder"]="/apps/bayesil/project/shared/BayesilWorking/Libraries/500"
env_dict["magmet_webserver"]["Quantify_lb_file"]="/apps/bayesil/project/shared/BayesilWorking/Libraries/filters/lower_bound_serum_exceptions.csv"
env_dict["magmet_webserver"]["SaveResultAsJson_confidence_scores_file"]="/apps/bayesil/project/shared/BayesilWorking/Libraries/filters/confidence_scores.txt"
env_dict["magmet_webserver"]["SaveSpectrumXYPairs_path"]="/apps/bayesil/project/releases/20180528215710/BayesilWorking/Results/4603a18207134a8b989492cf05faede748459be0/12152"
#env_dict["magmet_webserver"]["SaveSpectrumXYPairs_xy_file"]="xy_spectrum.txt"
env_dict["magmet_webserver"]["LibReading_filter_file"]="/apps/bayesil/project/shared/lib/bayesil/metabolites/serum.txt"
env_dict["magmet_webserver"]["LibReading_weight_list"]="weightSerum.csv"
env_dict["magmet_webserver"]["bayesil_Exponential_Line_Broadening"]=0.5






#######################################################
# Functions
#######################################################
def main(\
		bayesil_command,\
		run_bayesil,\
		working_dir,\
		python_4_magmet,\
		magmet_program,\
		biofluid,\
		library,\
		weight_list,\
		magmet_location,\
		dss_file_location,\
		):
	"""
	Main function
	"""
	verbose = 1
	if verbose >= 1:
		print "Running function Main()"
	project_name="magmet_profiling"

	run_bayesil_processing(\
		bayesil_command,\
		run_bayesil,\
		)

	x_y_spectrum="%s/xy_processed.txt" % working_dir
	if file_exists(x_y_spectrum)==0:
		print "ERROR!!! The spectrum %s was not generated" % x_y_spectrum
		print "Exiting..."
		sys.exit()

	run_magmet(\
		working_dir,\
		python_4_magmet,\
		magmet_program,\
		x_y_spectrum,\
		project_name,\
		biofluid,\
		library,\
		weight_list,\
		magmet_location,\
		dss_file_location,\
		)

	copy_results(\
		working_dir,\
		project_name,\
		)

	update_status_file(\
		working_dir,\
		)

	debug=1
	if debug==1:
		print "Exiting after function main()"
		sys.exit()
	return


def run_magmet(\
		working_dir,\
		python_4_magmet,\
		magmet_program,\
		x_y_spectrum,\
		project_name,\
		biofluid,\
		library,\
		weight_list,\
		magmet_location,\
		dss_file_location,\
		):
	"""
	Run Magmet profiling
/apps/bayesil/.pyenv/versions/anaconda-1.9.2/envs/py2715/bin/python  
python maple_main_19_march.py  
-i /apps/bayesil/magmet/MAGMET_June1st_2018/tests_at_home/nist_serum_from_tmic_bayesil/xy_before_profiling.txt  
-loc /apps/bayesil/magmet/MAGMET_June1st_2018/tests_at_home  


-p nist_serum_on_server_June_18_2018  
-b serum  
-l /apps/bayesil/magmet/MAGMET_June1st_2018/metabolite_list/serum_new.txt  
-w wt_list/serum_weight.xlsx   
-m /apps/bayesil/magmet/MAGMET_June1st_2018
-dss  /apps/bayesil/magmet/MAGMET_June1st_2018/metabolite_list/dss.txt
	"""
	verbose = 1
	if verbose >= 1:
		print "Running function run_magmet()"

	magmet_command=""


	if 	python_4_magmet=="":
		print "ERROR!!! python_4_magmet is empty. Exiting..."
		sys.exit()
	else:
		magmet_command+="%s " % python_4_magmet


	if 	magmet_program=="":
		print "ERROR!!! magmet_program is empty. Exiting..."
		sys.exit()
	else:
		magmet_command+="%s " % magmet_program


	if x_y_spectrum=="":
		print "ERROR!!! x_y_spectrum is empty. Exiting..."
		sys.exit()
	else:							
		magmet_command+=" -i %s " % x_y_spectrum


	if project_name=="":
		print "ERROR!!! project_name is empty. Exiting..."
		sys.exit()
	else:							
		magmet_command+=" -p %s " % project_name


	if working_dir=="":
		print "ERROR!!! working_dir is empty. Exiting..."
		sys.exit()
	else:							
		magmet_command+=" -loc %s " % working_dir


	if biofluid=="":
		print "ERROR!!! biofluid is empty. Exiting..."
		sys.exit()
	else:							
		magmet_command+=" -b %s " % biofluid


	if library=="":
		print "ERROR!!! library is empty. Exiting..."
		sys.exit()
	else:							
		magmet_command+=" -l %s " % library


	if weight_list=="":
		print "ERROR!!! weight_list is empty. Exiting..."
		sys.exit()
	else:							
		magmet_command+=" -w %s " % weight_list


	if magmet_location=="":
		print "ERROR!!! magmet_location is empty. Exiting..."
		sys.exit()
	else:							
		magmet_command+=" -m %s " % magmet_location


	if dss_file_location=="":
		print "ERROR!!! dss_file_location is empty. Exiting..."
		sys.exit()
	else:							
		magmet_command+=" -dss %s " % dss_file_location




	cur_dir=os.getcwd()
	os.chdir(magmet_location)

	print "MAGMET command: ", magmet_command
	os.system(magmet_command)

	os.chdir(cur_dir)
	debug=0
	if debug==1:
		print "Exiting after function run_magmet()"
		sys.exit()
	return




def run_bayesil_processing(\
		bayesil_command,\
		run_bayesil,\
		):
	"""
	Run Bayesil processing
	"""
	verbose = 1
	if verbose >= 1:
		print "Running function run_bayesil_processing()"

	if run_bayesil==1:
		print "bayesil_command= ", bayesil_command
		os.system(bayesil_command)

	debug=0
	if debug==1:
		print "Exiting after function run_bayesil_processing()"
		sys.exit()
	return

def copy_results(\
		working_dir,\
		project_name,\
		):
	"""
	Run Bayesil processing


[bayesil@do-bayesil-120180201-s-2vcpu-4gb-nyc2-01 12156]$ ls -lrt magmet_profiling/Results/

total 2040
-rw-rw-r-- 1 bayesil bayesil   23567 Jun 19 08:04 out_xy_processed.txt_detection_all.csv
-rw-rw-r-- 1 bayesil bayesil   25543 Jun 19 08:04 out_xy_processed.txt_cluster_data_all_with_multiplication_factor.csv
-rw-rw-r-- 1 bayesil bayesil    1410 Jun 19 08:04 out_xy_processed.txt_result_all_conc.csv
-rw-rw-r-- 1 bayesil bayesil 2029697 Jun 19 08:04 out_xy_processed.txt_result.json


	"""
	verbose = 1
	if verbose >= 1:
		print "Running function copy_results()"

	link_to_json_file="%s/%s/Results/out_xy_processed.txt_result.json" % (working_dir,project_name)
	final_json_file="%s/result.json" % working_dir

	link_to_csv_file="%s/%s/Results/out_xy_processed.txt_result_all_conc.csv" % (working_dir,project_name)
	final_csv_file="%s/bayesil.csv" % working_dir

	if file_exists(link_to_json_file)==0:
		print "Warning!!! JSON file %s does not exist" % link_to_json_file
		print "Exiting..."
		sys.exit()
	else:
		copy_command="cp %s %s" % (link_to_json_file, final_json_file)
		print "copy_command=", copy_command		
		json_copy_result=os.system(copy_command)
		print "json_copy_result = ",	json_copy_result


	if file_exists(link_to_csv_file)==0:
		print "Warning!!! csv file %s does not exist" % link_to_csv_file
		print "Exiting..."
		sys.exit()
	else:
		copy_command="cp %s %s" % (link_to_csv_file, final_csv_file)	
		print "copy_command=", copy_command		
		csv_copy_result=os.system(copy_command)	
		print "csv_copy_result = ",	csv_copy_result


	debug=0
	if debug==1:
		print "Exiting after function copy_results()"
		sys.exit()
	return


def update_status_file(\
		    working_dir,\
		):
	"""
	Run update status file
	"""
	verbose = 1
	if verbose >= 1:
		print "Running function update_status_file()"

	status_file="%s/status" %  working_dir
	if file_exists(status_file)==1:
		remove_file_command="rm %s" %  status_file
		os.system(remove_file_command)
	text="Complete"	
	write_file(status_file, text)				

	debug=0
	if debug==1:
		print "Exiting after function update_status_file()"
		sys.exit()
	return


#######################################################
# Pasing arguments
#######################################################

command_text="python "
bayesil_command=""
i=0

if len(sys.argv)>1:
	for item in sys.argv:
		##############################
		# Project/file management
		##############################
		#if item=="-l":
		#	location=os.path.abspath(sys.argv[a+1])
		#if item=="-p":
		#	project=sys.argv[a+1]
		if item=="-env":
			env=sys.argv[i+1]
			if env not in env_dict:
				print "Warning!!! env %s is not in env_dict" % env			
		#elif item=="-exp_dir":
		#	experiment_dir=sys.argv[a+1]
		#elif item=="-xml_dir":
		#	directory_with_xml_files=os.path.abspath(sys.argv[a+1])

		if item in ["-h", "-help"]:
			print help
			sys.exit()

		##############################
		# Magmet arguments
		##############################		

		if item in ["-i", "-sp"]:
			# print "input file location", os.path.abspath(sys.argv[i+1])
			# if sys.argv[i+1][0]="-"
			#data_dir = os.path.abspath(sys.argv[i + 1])
			spectra_path = os.path.abspath(sys.argv[i + 1])


		if item in ["-o", "-out"]:
			out_put_name = os.path.abspath(sys.argv[i + 1])

		if item in ["-b", "-bio"]: #Done
			bio_fluid_type = sys.argv[i + 1]
			#print "bio_fluid type ", bio_fluid_type

		if item in ["-l", "-lib"]:
			library_file = os.path.abspath(sys.argv[i + 1])

		if item in ["-w", "-wt"]: # Done
			t_list_file = os.path.abspath(sys.argv[i + 1])

		if item in ["-c", "-con"]: #Done
			constraint_file = os.path.abspath(sys.argv[i + 1])

		if item in ["-s", "-sub"]:
			subtraction_arg = sys.argv[i + 1]
			if subtraction_arg in ["yes","Yes"]:
				subtraction = "Yes"
				#print "Please provide high_conc_metabolites, subtraction_check_list_file "

		if item in ["-t", "-tail"]:
			tail_corr_arg = sys.argv[i + 1]
			if tail_corr_arg in ["yes","Yes","y"]:
				tail_corr = "Yes"
        		#print "Please provide conc_cutoff list. Other wise default [1000, 500, 100] will be used. "

		if item in ["-cl", "-c_list"]:
			cut_off_list = sys.argv[i + 1]



		if item in ["-hi"]:
			high_conc_metabolites = os.path.abspath(sys.argv[i + 1])
		if item in ["-hc"]:
			subtraction_check_list_file = os.path.abspath(sys.argv[i + 1])

		if item in ["-bc","-base"]:
			baseline_correction = os.path.abspath(sys.argv[i + 1])
			#print "baseline_correction ",baseline_correction	

		if item in ["-cutoff"]:
			conc_cutoff= sys.argv[i + 1]

		if item in ["-dss"]:
			dss_path = os.path.abspath(sys.argv[i + 1])

		if item in ["-f"]:  # Done
			field_strength = sys.argv[i + 1]
			print "field_strength ", field_strength

		if item in ["-bc","-base"]:
			baseline_correction = os.path.abspath(sys.argv[i + 1])
			#print "baseline_correction ",baseline_correction



		#Mark:
		if item in ["-m"]: # Done
			magmet_dir  = os.path.abspath(sys.argv[i + 1])

		#Mark:
		if item in ["-m_xml"]:
			magmet_xml_dir  = os.path.abspath(sys.argv[i + 1])

		#Mark:
		if item in ["-b_xml"]:
			bayesil_xml_dir  = os.path.abspath(sys.argv[i + 1])

		# Mark:
		if item in ["-loc"]:
			location  = os.path.abspath(sys.argv[i + 1])

		# Mark:
		if item in ["-p"]:
			project  = sys.argv[i + 1]




		##############################
		# Baeysil arguments
		##############################		

		# Mark:

		if item in ["-rb"]:
			run_bayesil  = int(sys.argv[i + 1])		

		# Link to Mark's Bayesil wrapper
		if item in ["-bw"]:
			bayesil_wrapper  = sys.argv[i + 1]	


		# Link to Bayesil Python script, add to command separately
		if item in ["-bp"]:
			bayesil_program  = sys.argv[i + 1]		


		# Link to Bayesil web profile, add to command separately
		if i==2:		
			bayesil_web_profile=os.path.abspath(item)
		# Overwriting Bayesil web profile file if "-wp" flag is used
		if item in ["-wp"]:
			bayesil_web_profile=sys.argv[i + 1]	

		# Path Bayesil settings	
		if item in ["--setting"]:
			bayesil_setting=os.path.abspath(sys.argv[i + 1])
			bayesil_command+=" --setting %s " % bayesil_setting

		# Bayesil update flag	
		if item in ["--update"]:
			bayesil_update=1
			bayesil_command+=" --update "

		if "ReadIOSpectrum.path" in item and "=" in item:			
			bayesil_spectrum_dir=os.path.abspath(item.split("=")[1])
			#bayesil_command+=" %s " % item

		if "Quantify.path" in item and "=" in item:			
			bayesil_quantify_path=os.path.abspath(item.split("=")[1])
			#bayesil_command+=" %s " % item

		if "Quantify.dss_conc" in item and "=" in item:
			bayesil_dss_conc_string=item.split("=")[1]
			if is_number(bayesil_dss_conc_string)==1:			
				bayesil_dss_conc=float(bayesil_dss_conc_string)
				#bayesil_command+=" %s " % item
			else:
				print "Warning!!! Bayesil Quantify.dss_conc is misformatted and is ", [item]	

		if "SaveStatus.path" in item and "=" in item:
			bayesil_SaveStatus_path=os.path.abspath(item.split("=")[1])
			#bayesil_command+=" %s " % item	

		if "SimplePlot.path" in item and "=" in item:
			bayesil_SimplePlot_path=os.path.abspath(item.split("=")[1])
			#bayesil_command+=" %s " % item

		if "SaveResultAsJson.json_file" in item and "=" in item:
			bayesil_SaveResultAsJson_json_file=os.path.abspath(item.split("=")[1])
			#bayesil_command+=" %s " % item			

		if "LibReading.lib_folder" in item and "=" in item:
			bayesil_LibReading_lib_folder=os.path.abspath(item.split("=")[1])
			#bayesil_command+=" %s " % item		

		if "Quantify.lb_file" in item and "=" in item:
			bayesil_Quantify_lb_file=os.path.abspath(item.split("=")[1])
			#bayesil_command+=" %s " % item			

		if "SaveResultAsJson.confidence_scores" in item and "=" in item:
			bayesil_SaveResultAsJson_confidence_scores_file=os.path.abspath(item.split("=")[1])
			#bayesil_command+=" %s " % item		

		if "SaveSpectrumXYPairs.path" in item and "=" in item:
			bayesil_SaveSpectrumXYPairs_path=os.path.abspath(item.split("=")[1])
			#bayesil_command+=" %s " % item			

		if "LibReading.filter_file" in item and "=" in item:
			bayesil_LibReading_filter_file=os.path.abspath(item.split("=")[1])
			#bayesil_command+=" %s " % item			

		if "LibReading.weight_list" in item and "=" in item:
			bayesil_LibReading_weight_list=item.split("=")[1]
			#bayesil_command+=" %s " % item	

		if "ExponentialLineBroadening.LB" in item and "=" in item:
			bayesil_Exponential_Line_Broadening_string=item.split("=")[1]
			if is_number(bayesil_Exponential_Line_Broadening_string)==1:			
				bayesil_Exponential_Line_Broadening=float(bayesil_Exponential_Line_Broadening_string)
				#bayesil_command+=" %s " % item
			else:
				print "Warning!!! Bayesil ExponentialLineBroadening.LB is misformatted and is ", [item]	

		if "=" in item:
			bayesil_command+=" %s " % item
			for argument in bayesil_argument_list:
				if argument not in item:
					print "Warning!!!! Argument %s is not in bayesil_argument_list"	%  item					

		i+=1
	command_text+=" %s " % item


###########################################
# End of pasing arguments
#######################################################



#######################################################
#  Processing general arguments
#######################################################							
this_script_full_path=os.path.abspath(sys.argv[0])
this_script_name=os.path.basename(sys.argv[0])
script_location=os.path.dirname(this_script_full_path)
if g_verbose>0:
	print "this_script_full_path=", this_script_full_path
	print "this_script_name=", this_script_name
	print "script_location=", script_location
cur_dir=os.getcwd()
if location=="":
	location=cur_dir
project_dir="%s/%s" % (location,project)

#######################################################
#  End of processing general arguments
#######################################################	

##################################
# Processing Magmet arguments
##################################


if magmet_dir!="":
	magmet_location=magmet_dir
else:
	if env not in env_dict:
		print "Warning!!! env %s is not in env_dict" % env
	else:
		if "magmet_location" not in env_dict[env]:
			print "Warning!!! magmet_location is not in env_dict[%s]" % env
		else:
			magmet_location=env_dict[env]["magmet_location"]
if magmet_location!="":
	d_magmet_dir=magmet_location
else:
	d_magmet_dir=os.getcwd()		


##################################
# Biofluid
##################################
d_bio_fluid_type="serum"

if "serum" in bayesil_LibReading_filter_file:
	bio_fluid_type="serum"

if bio_fluid_type=="":
	if "magmet_program" in env_dict[env]:
		magmet_program=env_dict[env]["magmet_program"]	

if bio_fluid_type=="":
	bio_fluid_type=d_bio_fluid_type
	

##################################


##################################
# Weight list
##################################
d_wt_list_file = d_magmet_dir + "/wt_list/serum_weight.xlsx"
##################################


##################################
# Constraint file
##################################
d_constraint_file = d_magmet_dir + "/constrain_list/constrain_serum.xlsx"
##################################


##################################
# Library file
##################################
d_library_file = d_magmet_dir + "/metabolite_list/serum.txt"
##################################


##################################
# magmet XML dir
##################################
d_magmet_xml_dir = d_magmet_dir + "/magmet_xml_dir/"
##################################


##################################
# Bayesil XML dir
##################################
d_bayesil_xml_dir = d_magmet_dir + "/bayesil_xml_dir/"
##################################


##################################
# Dss list
##################################
d_dss_path = d_magmet_dir + "/metabolite_list/dss.txt"
##################################

##################################
# check for tail_correction
##################################
d_tail_corr = "No"
##################################

##################################
# check for cutoff list
##################################
d_cut_off_list = [1000, 500,200]
##################################



##################################
# check for subtraction
##################################
d_subtraction = "No"
##################################


##################################
# high conc metabolites
##################################
d_high_conc_metabolites = d_magmet_dir + "/high_conc/beetle_high_conc.txt"
##################################



##################################
# subtraction_check_list_file
##################################
d_subtraction_check_list_file = d_magmet_dir + "/high_conc/subtraction_check_list.txt"
##################################

##################################
# concentration cutoff
##################################
d_conc_cutoff = 3000
##################################



##################################
# Spectra path
##################################
d_spectra_path = "/Volumes/GoogleDrive/Team Drives/magmet_python/bayesil_2/data/S1-1.fid.txt"
##################################

##################################
# out_put name
##################################
d_out_put_name = "out_"
##################################

#################################
# field_strength
##################################
field_strength = ""
d_field_strength = 700.23
##############################


if "python_for_magmet" in env_dict[env]:
	python_for_magmet=env_dict[env]["python_for_magmet"]
else:
	print "ERROR!!! python_for_magmet is not in env_dict[%s]" % env
	print "Exiting..."
	sys.exit()

if "magmet_program" in env_dict[env]:
	magmet_program=env_dict[env]["magmet_program"]
else:
	print "ERROR!!! magmet_program is not in env_dict[%s]" % env
	print "Exiting..."
	sys.exit()

if "library_file" in env_dict[env]:
	library=env_dict[env]["library_file"]
else:
	library=d_library_file
	print "Warning!!!! library_file is not in env_dict[%s]" % env
	print "library=d_library_file", d_library_file


if "wt_list_file" in env_dict[env]:
	weight_list=env_dict[env]["wt_list_file"]
else:
	weight_list=d_wt_list_file
	print "Warning!!!! wt_list_file is not in env_dict[%s]" % env
	print "weight_list=d_wt_list_file", d_wt_list_file


if "dss_path" in env_dict[env]:
	dss_file_location=env_dict[env]["dss_path"]
else:
	dss_file_location=d_dss_path
	print "Warning!!!! dss_path is not in env_dict[%s]" % env
	print "dss_file_location=d_dss_path", d_dss_path


y="""
              iteration = 20,
              iteration_i = 10,
              cut_off_list = [1000, 500, 200, 100, 50],
              noise_mask_list = [10,1,0.2],

env_dict["magmet_webserver"]["python_for_magmet"]="/apps/bayesil/.pyenv/versions/anaconda-1.9.2/envs/py2715/bin/python"
env_dict["magmet_webserver"]["magmet_program"]="maple_bruker_process_profile.py"
env_dict["magmet_webserver"]["magmet_location"]="/apps/bayesil/magmet"
env_dict["magmet_webserver"]["biofluid"]="serum"
env_dict["magmet_webserver"]["wt_list_file"]=env_dict["magmet_webserver"]["magmet_location"]+"/wt_list/serum_weight.xlsx"
env_dict["magmet_webserver"]["constraint_file"]=env_dict["magmet_webserver"]["magmet_location"]+"/constrain_list/constrain_serum.xlsx"
env_dict["magmet_webserver"]["library_file"]=env_dict["magmet_webserver"]["magmet_location"]+"/metabolite_list/serum.txt"
env_dict["magmet_webserver"]["magmet_xml_dir"]=env_dict["magmet_webserver"]["magmet_location"]+"/maple_xml_dir/"
env_dict["magmet_webserver"]["bayesil_xml_dir"]=env_dict["magmet_webserver"]["magmet_location"]+"/bayesil_xml_dir/"
env_dict["magmet_webserver"]["dss_path"]=env_dict["magmet_webserver"]["magmet_location"]+"/metabolite_list/dss.txt"
env_dict["magmet_webserver"]["high_conc_metabolites"]=env_dict["magmet_webserver"]["magmet_location"]+"/high_conc/beetle_high_conc.txt"
env_dict["magmet_webserver"]["subtraction_check_list_file"]=env_dict["magmet_webserver"]["magmet_location"]+"/high_conc/subtraction_check_list.txt"


env_dict["magmet_webserver"]["tail_corr"]="tail_corr"
env_dict["magmet_webserver"]["cut_off_list"]="1000,500,200"
env_dict["magmet_webserver"]["subtraction"]="No"
env_dict["magmet_webserver"]["conc_cutoff"]=3000
env_dict["magmet_webserver"]["spectra_path"]=""
env_dict["magmet_webserver"]["out_put_name"]="out_"
env_dict["magmet_webserver"]["field_strength"]=700.23
env_dict["magmet_webserver"]["baseline_correction"]=None
env_dict["magmet_webserver"]["iteration"]=20
env_dict["magmet_webserver"]["iteration_i"]=10
env_dict["magmet_webserver"]["cut_off_list"]="1000,500,200,100,50"
env_dict["magmet_webserver"]["noise_mask_list"]="10,1,0.2"		        
              """
 

#######################################################
#  End of processing Magmet arguments
#######################################################	


#######################################################
#  Processing Bayesil arguments
#######################################################	


#######################################################
#  Setting a job file for Bayesil
#######################################################	


bayesil_web_profile_2_use=""

if env not in env_dict:
	print "Warning!!! env %s is not in env_dict" % env
else:
	if "link_to_job_file" not in env_dict[env]:
		print "Warning!!! link_to_job_file is not in env_dict[%s]" % env
	else:
		bayesil_web_profile_2_use=env_dict[env]["link_to_job_file"]

if bayesil_web_profile_2_use=="" and bayesil_web_profile!="":
	bayesil_web_profile_2_use=bayesil_web_profile

if bayesil_web_profile_2_use=="":
	print "ERROR!!! bayesil_web_profile  was not found"
	print "The program will exit now"
	sys.exit()
else:
	bayesil_command=" %s " % bayesil_web_profile_2_use + bayesil_command





#######################################################
#  Setting Bayesil Python program
#######################################################	


bayesil_program_2_use=""

if env not in env_dict:
	print "Warning!!! env %s is not in env_dict" % env
else:
	if "link_to_bayesil_program" not in env_dict[env]:
		print "Warning!!! link_to_bayesil_program is not in env_dict[%s]" % env
	else:
		bayesil_program_2_use=env_dict[env]["link_to_bayesil_program"]


if bayesil_program_2_use=="" and bayesil_program!="":
	bayesil_program_2_use=bayesil_program
if bayesil_program_2_use=="":
	print "ERROR!!! bayesil_program  was not found"
	print "The program will exit now"
	sys.exit()
else:
	bayesil_command="python %s " % bayesil_program_2_use + bayesil_command

if bayesil_quantify_path=="":
	print "ERROR!!! bayesil_quantify_path  was not found"
	print "The program will exit now"
	sys.exit()	
else:	
	working_dir=bayesil_quantify_path
#######################################################
#  End of processing arguments
#######################################################	





#######################################################
#  Adding line broadening to bayesil command
#######################################################	

#if bayesil_Exponential_Line_Broadening_flag not in bayesil_command:
#	if "bayesil_Exponential_Line_Broadening" in env_dict[env]:
#		bayesil_Exponential_Line_Broadening_string=env_dict[env]["bayesil_Exponential_Line_Broadening"]
#		if is_number(bayesil_Exponential_Line_Broadening_string)==0:
#			print "Warning!!! bayesil_Exponential_Line_Broadening is not numerical and is ",[bayesil_Exponential_Line_Broadening_string]
#		else:
#			bayesil_Exponential_Line_Broadening_float=float(bayesil_Exponential_Line_Broadening_string)
#			Line_Broadening_argument="%s%s" % (bayesil_Exponential_Line_Broadening_flag,bayesil_Exponential_Line_Broadening_float)
#			bayesil_command=bayesil_command + " %s " % Line_Broadening_argument

#######################################################
#  Main function
#######################################################	


main(\
		bayesil_command,\
		run_bayesil,\
		working_dir,\
		python_for_magmet,\
		magmet_program,\
		bio_fluid_type,\
		library,\
		weight_list,\
		magmet_location,\
		dss_file_location,\
		)
print "The Magmet/Bayesil wrapper has finished"